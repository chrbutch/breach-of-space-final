using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pushback : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
        //Rigidbody playerRigidBody = collision.rigidbody;
        if (collision.gameObject.name == "Player w_ Camera")
		{
            Vector3 localRotation = collision.transform.localEulerAngles;
            Vector3 knockBackDir = transform.rotation.ToEulerAngles();

            float forcefieldFace = knockBackDir.y;
            forcefieldFace = forcefieldFace % 360;

            if (forcefieldFace < 0)
            {
				forcefieldFace = forcefieldFace * -1;
				if (forcefieldFace > 180)
				{
					forcefieldFace -= 180;
					if (forcefieldFace > 90)
					{
						forcefieldFace = 180 - forcefieldFace;
						knockBackDir.z = Mathf.Cos(forcefieldFace);
					}
					else
					{
						knockBackDir.z = Mathf.Cos(forcefieldFace);
					}
				}
				else
				{
					if (forcefieldFace > 90)
					{
						forcefieldFace = 180 - forcefieldFace;
						knockBackDir.z = -Mathf.Cos(forcefieldFace);
					}
					else
					{
						knockBackDir.z = -Mathf.Cos(forcefieldFace);
					}
				}

				if (270 > forcefieldFace && forcefieldFace > 90)
				{
					if (forcefieldFace > 180)
					{
						forcefieldFace = 270 - forcefieldFace;
						knockBackDir.x = -Mathf.Sin(forcefieldFace);
					}
					else
					{
						knockBackDir.x = -Mathf.Sin(forcefieldFace);
					}
				}
				else
				{
					if (forcefieldFace >= 270)
					{
						forcefieldFace = 360 - forcefieldFace;
						knockBackDir.x = Mathf.Sin(forcefieldFace);
					}
					else
					{
						knockBackDir.x = Mathf.Sin(forcefieldFace);
					}
				}
            }
			else
			{
            	if (forcefieldFace > 180)
				{
					forcefieldFace -= 180;
					if (forcefieldFace > 90)
					{
						forcefieldFace = 180 - forcefieldFace;
						knockBackDir.x = -Mathf.Cos(forcefieldFace);
					}
					else
					{
						knockBackDir.x = -Mathf.Cos(forcefieldFace);
					}
				}
				else
				{
					if (forcefieldFace > 90)
					{
						forcefieldFace = 180 - forcefieldFace;
						knockBackDir.x = Mathf.Cos(forcefieldFace);
					}
					else
					{
						knockBackDir.x = Mathf.Cos(forcefieldFace);
					}
				}

				if (270 > forcefieldFace && forcefieldFace > 90)
				{
					if (forcefieldFace > 180)
					{
						forcefieldFace = 270 - forcefieldFace;
						knockBackDir.z = Mathf.Sin(forcefieldFace);
					}
					else
					{
						knockBackDir.z = Mathf.Sin(forcefieldFace);
					}
				}
				else
				{
					if (forcefieldFace >= 270)
					{
						forcefieldFace = 360 - forcefieldFace;
						knockBackDir.z = -Mathf.Sin(forcefieldFace);
					}
					else
					{
						knockBackDir.z = -Mathf.Sin(forcefieldFace);
					}
				}
			}
            knockBackDir.y = 0;
            collision.rigidbody.AddForce(knockBackDir * 14000f);

          //  Debug.Log("Player is being pushed back");
		}
	}
}
