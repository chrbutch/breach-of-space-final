using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CutsceneHelper : MonoBehaviour
{
    public PlayableDirector director;
    public PlayableAsset[] playables;
    public GameObject robot;    // Cutscene Robot
    public FMODUnity.StudioEventEmitter emitter;
    public EPOOutline.Outlinable poi; // Maybe don't need?!
    Vector3 finalPos = new Vector3(50.0f, 0f, 144.91f); // Look, I don't love it either, but transform.forward is the wrong direction.
    public static int sceneNumber;
    public static bool ready;

    // Update is called once per frame
    void Update()
    {
        if (robot.activeInHierarchy)
        {
            robot.transform.position = Vector3.Lerp(robot.transform.position, finalPos, Time.deltaTime * .1f);
        }

        if (ready)
        {
            BeginTimeline();
            ready = false;
        }
    }

    public void MakeTheCall() => emitter.Play();
    public void BootSequence() => AudioHandler.gameMusic.start();
    public void ProgRock(int q) => AudioHandler.gameMusic.setParameterByName("Intro", q);

    public void BeginTimeline()
    {
        director.playableAsset = (sceneNumber == 0) ? playables[0] : playables[1];
        PlayerMove.pausePlayer = true;
        director.Play();
    }

    public void EndTimeline()
    {
        PlayerMove.pausePlayer = false;
    }

    // OTHER METHODS FOR TURNING ON THE NEXT OBJECTIVE?!

}
