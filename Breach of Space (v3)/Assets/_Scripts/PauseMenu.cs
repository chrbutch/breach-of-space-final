using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public static bool SettingsOpen = false;

    public GameObject pauseMenuUI;
    public GameObject settingsMenu;
    private void Start()
    {
        GameIsPaused = false;
        SettingsOpen = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !PlayerMove.pausePlayer)
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
                pauseMenuUI.SetActive(true);
            }
        }

        if (settingsMenu.activeInHierarchy && GameIsPaused)
        {
            pauseMenuUI.SetActive(false);
        }
        if (pauseMenuUI.activeInHierarchy && GameIsPaused)
        {
            settingsMenu.SetActive(false);
        }

        if (pauseMenuUI.activeInHierarchy || settingsMenu.activeInHierarchy)
        {
            GameIsPaused = true;
            Pause();
        }

    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        settingsMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        AudioHandler.Unpause();

        // Lock the mouse and make it invisible
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void Pause()
    {
        //pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        AudioHandler.Pause();

        // Unlock the mouse and make it visible
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    } 
    public void Settings()
    {
        settingsMenu.SetActive(true);
        pauseMenuUI.SetActive(false);
        
    }
    public void LoadMenu()
    {

        //Time.timeScale = 1f;  //<<<<<---------- Is this messing with things?
        SceneManager.LoadScene("NewMainMenu");
        // Debug.Log("Loading menu...");
    }
    public void QuitGame()
    {
        //Debug.Log("Quitting game...");
        Application.Quit();
        AudioHandler.FadeOutLevelMusic();
    }
    public void ExitSettings()
    {
        pauseMenuUI.SetActive(true);
        settingsMenu.SetActive(false);

    }
}

