using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Last updated by Christopher Butcher - Feb. 26th, 2022 (10:18 PM)
public class AudioHandler : MonoBehaviour
{
    public static int failed;
    public static float playerSeen;

    public static FMOD.Studio.EventInstance gameMusic;
    public string eventName;

    public static float MUSIC_VOLUME    = 1f;
    public static float SFX_VOLUME      = 1f;
    public static float MASTER_VOLUME   = 1f;

    static FMOD.Studio.Bus MusicBus;
    static FMOD.Studio.Bus SFXBus;
    static FMOD.Studio.Bus MasterBus;

    // Start is called before the first frame update
    private void Awake()
    {
        MusicBus =  FMODUnity.RuntimeManager.GetBus("bus:/Master/Music");
        SFXBus =    FMODUnity.RuntimeManager.GetBus("bus:/Master/SFX");
        MasterBus = FMODUnity.RuntimeManager.GetBus("bus:/Master");
    }

    void Start()
    {
        //gameMusic.start(); //<------------------ Removed to call from Timelines.
        playerSeen = 0;
        failed = 0;
        gameMusic = FMODUnity.RuntimeManager.CreateInstance(eventName);
    }

    private void Update()
    {
        SetParameters();
    }

    static void SetParameters()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("PlayerSeen", playerSeen);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("GameOver", (float)failed);
    }

    /// <summary>
    /// When player loses or the scene is reloaded,
    ///     this must be called in order to not
    ///     have multiple songs playing in scene.
    /// </summary>
    public static void FadeOutLevelMusic()
    {
        gameMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        gameMusic.release();
        gameMusic.clearHandle();
    }

    public static void GameOver(bool win)
    {
        failed = win ? 0 : 1;
        SetParameters();
    }

    public static void PlaySound(string path, Vector3 pos) => FMODUnity.RuntimeManager.PlayOneShot(path, pos);

    public static void Pause() => FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Paused", 1f);

    public static void Unpause() => FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Paused", 0f);

    public static void SetMusicVolume(float vol)
    {
        MUSIC_VOLUME = vol;
        MusicBus.setVolume(MUSIC_VOLUME);
    }

    public static void SetSFXVolume(float vol)
    {
        SFX_VOLUME = vol;
        SFXBus.setVolume(SFX_VOLUME);
    }

    public static void SetMasterVolume(float vol)
    {
        MASTER_VOLUME = vol;
        MasterBus.setVolume(MASTER_VOLUME);
    }

    public static void PlayerSeen(bool hasPlayerBeenSeen) => playerSeen = hasPlayerBeenSeen ? 1.0f : 0.0f;

    public static void ChangeSong(string newSong)
    {
        FadeOutLevelMusic();
        gameMusic.clearHandle();
        MusicBus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        gameMusic = FMODUnity.RuntimeManager.CreateInstance(newSong);
        gameMusic.start();
    }

    internal static void StopEverything() => MasterBus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    
}
