using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndUI : MonoBehaviour
{
    //public GameObject menu;
    public GameObject gO;
    public GameObject noiseHud;
    public GameObject detectionHud;

    
    void Awake()
    {
        gO = GameObject.FindGameObjectWithTag("CaughtUI");
        noiseHud = GameObject.Find("Noise Maker HUD");
        detectionHud = GameObject.Find("Detection");
    }
    void Start()
    {
        //menu.SetActive(false);
        gO.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(/*menu.activeInHierarchy == true || */gO.activeInHierarchy == true)
        {
            PauseGame();
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            UnpauseGame();
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void TurnOffHUD()
    {
        noiseHud.SetActive(false);
        detectionHud.SetActive(false);
    }

    public void TurnOnHUD()
    {
        noiseHud.SetActive(true);
        detectionHud.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    /*public void ShowMenu()
    {
        menu.SetActive(true);
    }*/

    public void GameOver()
    {
        gO.SetActive(true);
        TurnOffHUD();
        AudioHandler.GameOver(false);
    }

    public void ReloadScene()
    {
        //This should reload to the start of the game or the start menu.
        AudioHandler.FadeOutLevelMusic();
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
    }

}
