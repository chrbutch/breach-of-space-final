using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayerData(PlayerData player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerData.bos";
        FileStream stream = new FileStream(path, FileMode.Create);
        Debug.Log("SAVED TO : " + path); // <-------------------------------
        SaveData data = new SaveData(player);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SaveData LoadSaveData()
    {
        string path = Application.persistentDataPath + "/playerData.bos";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            SaveData data = (SaveData)formatter.Deserialize(stream);
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("No such save file at " + path);
            return null;
        }
    }
}
