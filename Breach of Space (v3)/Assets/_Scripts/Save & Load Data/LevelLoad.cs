using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoad : MonoBehaviour
{
    public Slider slider;
    public Text progressText;
    //public bool isLoadsceneLoaded = false;


    private void Start()
    {

        LoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsych(sceneIndex)); 
    }



    IEnumerator LoadAsych (int sceneIndex)
    {
        AsyncOperation currentLoad = SceneManager.LoadSceneAsync(sceneIndex);

        while (!currentLoad.isDone)
        {
            //yield return WaitUntil();

            float progress = Mathf.Clamp01(currentLoad.progress / 0.9f);

            slider.value = progress;

            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }

}
