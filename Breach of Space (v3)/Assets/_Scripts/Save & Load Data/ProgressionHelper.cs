using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionHelper : MonoBehaviour
{
    [Header("Area One")]
    public GameObject[] areaOne;
    [Header("Area Two")]
    public GameObject[] areaTwo;
    [Header("Area Three")]
    public GameObject[] areaThree;
    [Header("Area Four")]
    public GameObject[] areaFour;

    public void OpenAreas(int area)
    {
        switch (area)
        {
            case 4:
                foreach (GameObject g in areaFour)
                {
                    g.SetActive(false);
                }
                goto case 3;
            case 3:
                foreach (GameObject g in areaThree)
                {
                    g.SetActive(false);
                }
                goto case 2;
            case 2:
                foreach (GameObject g in areaTwo)
                {
                    g.SetActive(false);
                }
                goto case 1;
            case 1:
                foreach(GameObject g in areaOne)
                {
                    g.SetActive(false);
                }
                break;
            case 0:
                break;
            default:
                Debug.LogWarning("Something has gone wrong in ProgressionHelper.cs");
                break;
        }
    }
}
