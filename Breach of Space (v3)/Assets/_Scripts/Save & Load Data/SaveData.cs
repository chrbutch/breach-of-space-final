using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public short level;
    // resources, etc.

    public SaveData(PlayerData playerData)
    {
        this.level = playerData.level;
        // more info here.
    }
}
