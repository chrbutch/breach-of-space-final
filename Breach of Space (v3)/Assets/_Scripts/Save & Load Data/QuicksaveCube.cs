using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuicksaveCube : MonoBehaviour
{
    public LoadedArea area;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerData pData = other.GetComponent<PlayerData>();
            if (pData != null)
            {
                if(pData.level != area.areaNumber)
                {
                    if(area.areaNumber > 1)
                    {
                        AudioHandler.ChangeSong(area.songTitle);
                    }
                    pData.level = area.areaNumber;
                    pData.SavePlayer();
                }
            }
        }
    }
}
