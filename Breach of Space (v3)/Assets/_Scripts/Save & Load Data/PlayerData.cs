using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public short level;
    public LoadedArea[] areas;
    
    public void SavePlayer()
    {
        SaveSystem.SavePlayerData(this);
        SaveIcon.saving = true;
    }

    public void LoadPlayer()
    {
        SaveData sd = SaveSystem.LoadSaveData();
        if(sd == null)
        {
            SavePlayer();
            //LoadPlayer();
            return;
        }
        level = sd.level;
        areas[level].OnLoad();
    }
}
