using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveIcon : MonoBehaviour
{
    public static bool saving;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        saving = false;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (saving)
        {
            anim.ResetTrigger("Saved");
            anim.SetTrigger("Saved");
            saving = false;
        }
    }
}
