using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SafetyNet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("<color=magenta>PLAYER HAS FALLEN THROUGH THE FLOOR. RESTARTING SCENE.</color>");
            AudioHandler.FadeOutLevelMusic();
            AudioHandler.StopEverything();
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
