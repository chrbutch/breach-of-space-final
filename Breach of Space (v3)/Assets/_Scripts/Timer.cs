using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timelimit;
    public Text text;
    static bool loadedScene = false;

    public void ChangeScene(int changeTheScene)

    {
        //SceneManager.LoadScene(changeTheScene);
    }

    void Update()
    {

        timelimit -= Time.deltaTime;
        text.text = "" + Mathf.Round(timelimit);
        if (timelimit < 0)
        {
            timelimit = 0;
            loadedScene = true; //We have loaded Scene so mark it true
            SceneManager.LoadScene(0);
        }
    }
}
