using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDialogueBox : MonoBehaviour
{
    // Dialogue box attributes
    public GameObject dialogueBoxObject;

    // Timer attributes
    public float timeoutSeconds = 60f;
    private bool isTimerRunning = false;

    // Start is called before the first frame update
    void Start()
    {
        // Start the timer
        isTimerRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimerRunning && dialogueBoxObject != null)
		{
            // Start the countdown
            if (timeoutSeconds > 0)
			{
                if (timeoutSeconds - (int) timeoutSeconds < 0.01)
					//Debug.Log(dialogueBoxObject.name + " will disappear in " + (int)timeoutSeconds + " seconds");
                timeoutSeconds -= Time.deltaTime;
			}

            // If the time is up
            else
			{
                isTimerRunning = false;
                dialogueBoxObject.SetActive(false);
                //Debug.Log("Dialogue box should disappear now");
            }
		}
        else
        {
            Debug.Log("Dialogue box is null");
        }

    }
}
