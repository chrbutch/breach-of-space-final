using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FMODUnity;
using Panda;

// 2022.02.16 @ 07:26 PM - CB.
public class Robot : MonoBehaviour
{
    NavMeshAgent navAgent;
    Animator anim;
    Vector3 lastKnownPlayerPosition;

    public static GameObject player;
    public GameObject lossCameraHelper;

    public GameObject itemHand; // the area where Noisemaker (et al) attaches to the Robot's hand model.

    public Vector3 destination;

    public float speed;         // Used to set NavMesh speed.
    public float currentSpeed;  // 0 = idle, 3 = walking, 5 = running??
                                //  Used to determine Animation.
    [Range(0.0f, 1.0f)]
    public float suspicionLevel = 0f;

    public float angleCutoff = 60f;
    public float playerDistanceCutoff = 5f;

    public bool busy;
    bool aggro;

    public Waypoint[] patrolPositions;  // All regular Waypoints (Working & Patrol).
    public Waypoint currentWaypoint;    // Used to determine nature of Waypoint (could be Suspicious!).

    public float suspicionTimer = 5f;

    [Range(0, 50)]
    public float currentHearingDistance;

    int currentPatrolPosition = 0;      // Current Waypoint in patrolPositions.

    static float HEARING_DIST_REG   = 5f;
    static float HEARING_DIST_MID   = 10f;
    static float HEARING_DIST_MAX   = 15f;
    static float WALK_SPEED         = 2f;
    static float RUN_SPEED          = 5f;

    public RobotStates curState = new RobotStates();

    private bool isWorking;

    private static Color LOW_THREAT     = new Color(91f / 255f, 224f / 255f, 253f / 255f);
    private static Color MID_THREAT     = new Color(    1.0f  , 216f / 255f,  77f / 255f);
    private static Color HIGH_THREAT    = new Color(    1.0f  , 168f / 255f,  44f / 255f);

    public enum RobotStates
    {
        Idle,       Curious,    Chasing,
        Searching,  Walking,    Working,
        Trapped,    Dead,       PoweredDown,
        Deafened
    }

    // FMOD //
    FMOD.Studio.EventInstance robotVoice_AHA;
    FMOD.Studio.EventInstance robotVoice_HUH;
    FMOD.Studio.EventInstance robotVoice_BROKEN;
    FMOD.Studio.EventInstance robotVoice_GIVEUP;

    bool playingVoiceline;

    // FOR GDEX ONLY.
    // PAY NO MIND TO THE MAN BEHIND THE CURTAIN.
    public static bool cheating;

    public NoisemakerWaypoint RobotNoisemaker; // Used for Animation!

    public MeshRenderer POVmesh;
    Material POVshaderMaterial;
    Color greenlight = new Color((float)(8/255), (float)(64/ 255), 0);

    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.Warp(transform.position);
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponentInChildren<Animator>();

        robotVoice_AHA      = FMODUnity.RuntimeManager.CreateInstance("event:/s_Robot_AHA");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(robotVoice_AHA,     transform, GetComponent<Rigidbody>());
        robotVoice_HUH      = FMODUnity.RuntimeManager.CreateInstance("event:/s_Robot_HUH");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(robotVoice_HUH,     transform, GetComponent<Rigidbody>());
        robotVoice_BROKEN   = FMODUnity.RuntimeManager.CreateInstance("event:/s_Robot_BREAK");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(robotVoice_BROKEN,  transform, GetComponent<Rigidbody>());
        robotVoice_GIVEUP   = FMODUnity.RuntimeManager.CreateInstance("event:/s_Robot_GIVEUP");
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(robotVoice_GIVEUP,  transform, GetComponent<Rigidbody>());
        
        SetRobotStatus(RobotStates.Walking);

        if(patrolPositions.Length > 0)
        {
            SetRobotDestination(patrolPositions[0].transform.position);
            currentWaypoint = patrolPositions[0];
        }
        else
        {
            SetRobotDestination(this.transform.position);
            WaypointSafeguard();
        }

        navAgent.speed = WALK_SPEED;
        //currentHearingDistance = HEARING_DIST_REG;
        aggro = false;
        POVshaderMaterial = POVmesh.material;
        cheating = false; // DELETE ME AFTER GDEX.
    }

    void FixedUpdate()
    {
        // ------------------------------  BEGIN SIGHT  ------------------------------ 

        if (cheating)
        { // WE HAVE TO TAKE THIS OUT, PROBABLY -c.
            CHEAT();
            // DELETE ME AFTER GDEX PLEASE.
            if (NearPlayer(1.5f))
            {
                //print("FOUND YA BOYO");
                if (!PlayerMove.gameOver)
                {
                    //print("FOUND INNER STATEMENT");
                    PlayVoiceline("event:/s_Robot_CAUGHT");
                    AudioHandler.failed = 1;
                    // GAME OVER MAN <----------------------------------------------------- insert here.
                    //fancy camera shenanigans ensue
                    player.GetComponent<PlayerMove>().camController.gotCaught(gameObject);
                    player.GetComponent<PlayerMove>().Caught(gameObject.transform);
                    currentSpeed = 0;
                    Time.timeScale = 0;
                    PlayerMove.gameOver = true;
                }
                //make robot stop moving.
                SetRobotStatus(RobotStates.Idle);
                cheating = false;
                SetRobotDestination(this.transform.position);

            }
        }
        else
        {
            if (PlayerMove.gameOver)
            {
                SetRobotStatus(RobotStates.Idle);
                currentSpeed = 0;
            }
            else if (GetSuspicionLevel() < 0.5f)             // IF not super suspicious.
            {
                //Debug.Log("Sus < 0.5f");
                if (NearPlayer(playerDistanceCutoff))   // if player is nearby
                {
                    //Debug.Log("Sus < 0.5f & NearPlayer");
                    if (LookingAtPlayer())              // looking toward player
                    {
                        //Debug.Log("Sus < 0.5f & NearPlayer & Looking @ player");
                        if (PlayerSeen())               // player is visible
                        {
                            //Debug.Log("Sus < 0.5f & NearPlayer & Looking @ player & playerseen");
                            SetLastKnownPlayerDestination(player.transform.position);
                            IncreaseSuspicionLevel(PlayerVisibility(), playerDistanceCutoff);
                        }
                        else
                        {
                            //Debug.Log("PLAYER NOT SEEN");
                            // player has not been seen!
                            // but has it been heard?
                        }
                    }
                    else
                    {
                        //Debug.Log("PLAYER NOT IN FRONT OF ME");
                        //not looking at player
                        // but has it been heard?
                    }
                }
                else
                {
                    //Debug.Log("PLAYER TOO FAR");
                    // not near player
                    // but has there been a sabotage?
                }

                DecreaseSuspicionLevel();
            }
            else
            {
                if (GetSuspicionLevel() >= 1.0f)
                {
                    //Debug.Log("SuspicionLevel >= 1.0f");
                    if ((LookingAtPlayer() && PlayerSeen()) || (NearPlayer(playerDistanceCutoff) && PlayerSeen()))
                    {
                        //Debug.Log("LOOKING @ PLAYER & PLAYERSEEN");
                        NavMeshPath path = new NavMeshPath();
                        if (NavMesh.CalculatePath(this.transform.position, player.transform.position, 1, path))
                        {
                            if (curState != RobotStates.Chasing)
                            {
                                PlayVoiceline("event:/s_Robot_AHA");
                            }
                            SetRobotStatus(RobotStates.Chasing);
                            SetRobotDestination(player.transform.position);
                            SetLastKnownPlayerDestination(player.transform.position);
                            // Should busy be set here?
                            //Debug.Log("Sus == " + suspicionLevel);
                        }
                        else
                        {
                            SetRobotStatus(RobotStates.Searching);
                            //Debug.Log("<color=magenta>OH, SO THIS IS WHY.</color>");
                            // can't get to player!!!!!
                            //Debug.Log("AHHHHHHHHHHH");
                        }
                        IncreaseSuspicionLevel(PlayerVisibility(), playerDistanceCutoff);
                    }
                    else
                    {
                        //Debug.Log("LOOK @ PLAYER? : " + LookingAtPlayer());
                        //Debug.Log("PLAYER SEEN? : " + PlayerSeen());
                        NavMeshPath path = new NavMeshPath();
                        if (NavMesh.CalculatePath(this.transform.position, lastKnownPlayerPosition, 1, path))
                        {
                            //Debug.Log("&& FIRST ONE: on NavMesh");
                            //Debug.Log("ON NAVMESH");
                            SetRobotDestination(lastKnownPlayerPosition);
                            SetLastKnownPlayerDestination(lastKnownPlayerPosition);
                            DecreaseSuspicionLevel();

                            if (curState != RobotStates.Searching)
                            {
                                //Debug.Log("&& 2nd ONE: curState != Searching");
                                if (curState != RobotStates.Chasing)
                                {
                                    //Debug.Log("&& 3rd ONE: curState != Chasing");
                                    SetRobotStatus(RobotStates.Searching);
                                }
                                RobotBlackboard.ReportSuspiciousActivity(this, lastKnownPlayerPosition);
                            }

                            // Should busy be set here?
                            //Debug.Log("Sus == " + suspicionLevel);
                        }
                        else
                        {
                            //Debug.Log("&& 4th ONE: in the else");
                            SetLastKnownPlayerDestination(this.transform.position);
                            if (curState != RobotStates.Searching)
                            {
                                //Debug.Log("&& 5th ONE: curState != Searching");
                                if (curState != RobotStates.Chasing)
                                {
                                    //Debug.Log("&& 6th ONE: curState != Searching");
                                    SetRobotStatus(RobotStates.Searching);
                                }
                                RobotBlackboard.ReportSuspiciousActivity(this, lastKnownPlayerPosition);
                            }
                            //Debug.Log("&& OOPS");
                            if (GetSuspicionLevel() > 0)
                            {
                                //Debug.Log("&& last IF: DecreaseSusLvl");
                                DecreaseSuspicionLevel();
                            }
                        }
                    }

                    if (NearPlayer(1.5f))
                    {
                        //print("FOUND YA BOYO");
                        if (!PlayerMove.gameOver)
                        {
                            //print("FOUND INNER STATEMENT");

                            AudioHandler.failed = 1;
                            PlayVoiceline("event:/s_Robot_CAUGHT");
                            // GAME OVER MAN <----------------------------------------------------- insert here.
                            //fancy camera shenanigans ensue
                            player.GetComponent<PlayerMove>().camController.gotCaught(gameObject);
                            player.GetComponent<PlayerMove>().Caught(gameObject.transform);
                            currentSpeed = 0;
                            Time.timeScale = 0;
                            PlayerMove.gameOver = true;

                        }
                        //make robot stop moving.
                        SetRobotStatus(RobotStates.Idle);
                        SetRobotDestination(this.transform.position);
                        cheating = false;
                    }

                }
                else
                {
                    // if it's 0.5 - 0.9999999999999
                    if (LookingAtPlayer() && PlayerSeen()) // <-- NearPlayer = OK? v. HeardPlayer()???
                    {
                        //Debug.Log("THIS IS PROBABLY GETTING CALLED A LOT");
                        //Debug.Log("&& THIS WOULD DO IT?");
                        RobotBlackboard.ReportSuspiciousActivity(this, lastKnownPlayerPosition);
                        IncreaseSuspicionLevel(PlayerVisibility(), playerDistanceCutoff);
                    }
                    else
                    {
                        DecreaseSuspicionLevel();
                    }
                }
            }

            // ------------------------------   END SIGHT   ------------------------------

            // ------------------------------ BEGIN HEARING ------------------------------

            //AdjustHearingDistance(); // Adjust hearing every frame (lerping!)  <-------------- should this instead be called by things that make noises?????????????????????????
            // REMOVED THIS. NOT SURE WE NEED TO WORRY ABOUT HEARING MUCH ANYMORE

            if (curState != RobotStates.Deafened && NearPlayer(currentHearingDistance))
            {
                //Debug.Log("<color=magenta>GET PLAYER NOISE:</color><color=yellow> " + GetPlayerNoise() + "</color>");

                if (GetPlayerNoise() > 0) // or greater than some noise threshold.
                {
                    IncreaseSuspicionLevel(PlayerAudibility(), currentHearingDistance);
                }

            }
        }// DELETE THIS PAREN TO DISABLE CHEAT POST GDEX --------------------------------------------------------



        if (AtDestination() && !busy && GetSuspicionLevel() < 1f) //<-------------------- ??? BUSY ???
        {
            busy = true; 
            //currentWaypoint = patrolPositions[currentPatrolPosition];
            StartCoroutine(currentWaypoint.WaypointBehavior(this));
        }
        else
        {
            /*
            Debug.Log("<color=red> NOT AT DEST?: " + AtDestination().ToString() + "</color>");
            Debug.Log("<color=yellow> BUSY?: " + busy + "</color>");
            Debug.Log("<color=blue> SUSPICION > 0.5f?: " + GetSuspicionLevel().ToString() + "</color>"); */

            // @ dest & sus >=1:    looking at "sus" waypoint created when player lost.
            // @ dest & busy:       working on the current task.
            // busy & sus >=1:      WRONG.

            // do we need this???????????????????????
            // not @ destination: Auto move to dest
            // if not busy, what?
        }
    }

    // ----------------------------------------------------- \\
    // ----------------------------------------------------- \\
    // ----------------------------------------------------- \\

    public void SetNextRobotWaypoint()
    {
        currentPatrolPosition = (currentPatrolPosition + 1) % patrolPositions.Length;
        currentWaypoint = patrolPositions[currentPatrolPosition];
        Vector3 nextPosition = patrolPositions[currentPatrolPosition].transform.position;
        SetRobotDestination(nextPosition);
        busy = false;
    }

    /// <summary>
    /// Sets the destination of the Robot.
    /// </summary>
    /// <param name="nextPosition">The destination (a Vector3) that the Robot is moving to.</param>
    public void SetRobotDestination(Vector3 nextPosition)
    {
        NavMesh.SamplePosition(nextPosition, out NavMeshHit hit, 10, 1);
        destination = new Vector3(nextPosition.x, hit.position.y, nextPosition.z);
        navAgent.SetDestination(destination);

        //busy = false;
    }

    void AnimationUpdate()
    {
        anim.SetFloat("currentSpeed", speed);
        anim.SetBool("isWorking", isWorking);
    }

    public void SetRobotStatus(RobotStates newState)
    {
        this.curState = newState;

        switch (newState)
        {
            case RobotStates.Idle:
                speed = 0f;
                isWorking = false;
                break;
            case RobotStates.Chasing:
                speed = RUN_SPEED;
                isWorking = false;
                break;
            case RobotStates.Working:
                speed = 0f;
                isWorking = true;
                break;
            case RobotStates.Curious:
                speed = WALK_SPEED * 0.75f;
                isWorking = false;
                break;
            case RobotStates.Searching:
                speed = WALK_SPEED * 0.75f;
                isWorking = false;
                break;
            case RobotStates.PoweredDown:
                speed = 0f;
                isWorking = false;
                break;
            case RobotStates.Trapped:
                speed = 0f;
                isWorking = false;
                break;
            case RobotStates.Deafened:
                //???????????????????
                if(GetSuspicionLevel() < 0.5f)
                {
                    suspicionLevel = 0.5f;
                }
                isWorking = false;
                break;
            case RobotStates.Dead:
                speed = 0f;
                isWorking = false;
                // DEATH METHOD (DEATHOD).
                break;
            default:
                // walking, basically.
                speed = WALK_SPEED;
                isWorking = false;
                break;
        }

        AnimationUpdate();
        navAgent.speed = speed;
    }

    void WaypointSafeguard()
    {
        GameObject newWaypoint = new GameObject("New Waypoint", typeof(PatrolWaypoint));
        newWaypoint.transform.position = this.transform.position;
        newWaypoint.GetComponent<PatrolWaypoint>().timeHere = 20f;
        patrolPositions = new Waypoint[1];
        patrolPositions[0] = newWaypoint.GetComponent<PatrolWaypoint>();
        currentPatrolPosition = 0;
        currentWaypoint = patrolPositions[0];
        // Create a 2nd one to make a random path for the Robot??? -c
    } 
    public bool AtDestination() => (Vector3.Distance(transform.position, destination) <= navAgent.stoppingDistance);
    public void SetLastKnownPlayerDestination(Vector3 lastPlayerLocationSeen) => lastKnownPlayerPosition = lastPlayerLocationSeen;
    public bool NearPlayer(float cutoff) => (Vector3.Distance(transform.position, player.transform.position) <= cutoff);

    public bool LookingAtPlayer()
    {
        float cosAngle = Vector3.Dot(
            (player.transform.position - this.transform.position).normalized,
            this.transform.forward);
        float robotAngle = Mathf.Acos(cosAngle) * Mathf.Rad2Deg;
        return robotAngle < angleCutoff;
    }

    public float PlayerVisibility()
    {
        float result = 0f;

        foreach(GameObject g in RobotBlackboard.playerRaycastPoints)
        {
            RaycastHit hit;
            Vector3 eyeline = transform.position + new Vector3(0, .25f, 0);
            Vector3 normalizedDirection = (g.transform.position - eyeline).normalized;

            if(Physics.Raycast(eyeline, normalizedDirection, out hit, playerDistanceCutoff))
            {
                //Debug.DrawRay(eyeline, normalizedDirection, Color.cyan, 3f);
                //Debug.Log("%%% hit: " + hit.collider.name);
                if(hit.collider.tag == "Player")
                {
                    //Debug.Log("%%% HIT THE PLAYER!!!");
                    result += 0.2f;
                }
                else
                {
                    //Debug.Log("%%% HIT SOMETHING ELSE? " + hit.collider.tag);
                }
            }
        }
        result = Mathf.Clamp01(result);
        return result;
    }

    public float PlayerAudibility()
    {
        float result = 0;
        RaycastHit hearingHit;
        Vector3 earline = transform.position + new Vector3(0, .25f, 0);
        Vector3 normalizedHearingDirection = (player.transform.position - earline).normalized;
        
        if (Physics.Raycast(earline, normalizedHearingDirection, out hearingHit, currentHearingDistance))
        {
            if(hearingHit.collider.tag == "Player")
            {
                result += GetPlayerNoise();
            }
        }
        return result;
    }

    public void IncreaseSuspicionLevel(float suspiciousVariable, float cutoffVariable)
    {
        //Debug.Log("%%% Inc. Sus. Lvl.");
        if(GetSuspicionLevel() < 1)
        {
            //Debug.Log("Get Sus Lvl < 1");
            suspicionLevel += 
                (((cutoffVariable - Vector3.Distance(transform.position, player.transform.position))
                * suspiciousVariable) * (Time.deltaTime / cutoffVariable)/2);
                //@CHRIS I CHANGED THIS BY DIVIDING BY 2 ^^^^ -JACOB
            //Debug.Log(suspicionLevel);
            if(GetSuspicionLevel() > 0)
            {
                suspicionTimer = 5f;
            }
        }
        else
        {
            //Debug.Log("Sus lvl == ONE");
            suspicionLevel = 1;
            busy = false; //<------------------------
            if (!aggro)
            {
                aggro = true;
                //Debug.Log("<color=yellow>OH NO</color>..." + curState);
                BeginChasingPlayer();
            }

            if (suspicionTimer < 5)
            {
                suspicionTimer = 5f;
            }
        }

        suspicionLevel = Mathf.Clamp(suspicionLevel, 0, 1);
        // FUZZY MATHS
        //sus += (playerDistance.normalized * playerVisibility) * Time.deltaTime.
        LerpPOV(suspicionLevel);

    }

    public void DecreaseSuspicionLevel()
    {
        if (curState != RobotStates.Chasing) // && curState != RobotStates.Searching)
        {
            suspicionTimer -= Time.deltaTime;
            suspicionTimer = Mathf.Clamp(suspicionTimer, 0, 5);

            if (suspicionTimer <= 0)
            {
                if (GetSuspicionLevel() > 0)
                {
                    if (GetSuspicionLevel() >= 0.5f)
                    {
                        suspicionLevel -= Time.deltaTime / 2;
                    }
                    else
                    {
                        suspicionLevel -= Time.deltaTime;
                    }
                }
                else
                {
                    suspicionLevel = 0;

                    if (aggro)
                    {
                        aggro = false;
                        //Debug.Log("Current state is no longer Robot Chasing (should happen once).");
                        if (curState == RobotStates.Searching) 
                        {
                            SetRobotStatus(RobotStates.Walking);
                        }
                        anim.ResetTrigger("sawPlayer");
                        PlayVoiceline("event:/s_Robot_GIVEUP");
                        RobotBlackboard.robotsChasingYou--;
                        if(RobotBlackboard.robotsChasingYou <= 0)
                        {
                            AudioHandler.PlayerSeen(false);
                        }
                    }
                }
            }
        }
        suspicionLevel = Mathf.Clamp(suspicionLevel, 0, 1);
        LerpPOV(suspicionLevel);
    }

    public float GetSuspicionLevel() => Mathf.Clamp01(this.suspicionLevel);

    void PlaySound(string path)
    {
        //Debug.Log("PATH: " + path);
        FMODUnity.RuntimeManager.PlayOneShot(path, transform.position);
    }

    public void PlayVoiceline(string path)
    {
        if (!PlayerMove.gameOver)// && !playingVoiceline)
        {
            switch (path)
            {
                case "event:/s_Robot_AHA":
                    //playingVoiceline = true;
                    robotVoice_AHA.getPlaybackState(out FMOD.Studio.PLAYBACK_STATE AHA_STATE);
                    //Debug.Log(AHA_STATE.ToString() + "= AHA PLAYBACK STATE");
                    if (AHA_STATE == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                    {
                        //Debug.Log("AHA IF PLAYBACK STOPPED");
                        robotVoice_AHA.start();
                    }
                    robotVoice_BROKEN.release();
                    robotVoice_GIVEUP.release();
                    robotVoice_HUH.release();
                    break;
                case "event:/s_Robot_HUH":
                    //playingVoiceline = true;
                    robotVoice_HUH.getPlaybackState(out FMOD.Studio.PLAYBACK_STATE HUH_STATE);
                    //Debug.Log(HUH_STATE.ToString() + "= HUH PLAYBACK STATE");
                    if (HUH_STATE == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                    {
                        robotVoice_HUH.start();
                    }
                    robotVoice_BROKEN.release();
                    robotVoice_GIVEUP.release();
                    robotVoice_AHA.release();
                    break;
                case "event:/s_Robot_BROKEN":
                    //playingVoiceline = true;
                    robotVoice_BROKEN.getPlaybackState(out FMOD.Studio.PLAYBACK_STATE BROKEN_STATE);
                    //Debug.Log(BROKEN_STATE.ToString() + "= BROKEN PLAYBACK STATE");
                    if (BROKEN_STATE == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                    {
                        robotVoice_BROKEN.start();
                    }
                    robotVoice_HUH.release();
                    robotVoice_GIVEUP.release();
                    robotVoice_AHA.release();
                    break;
                case "event:/s_Robot_GIVEUP":
                    //playingVoiceline = true;
                    robotVoice_GIVEUP.getPlaybackState(out FMOD.Studio.PLAYBACK_STATE GIVEUP_STATE);
                    //Debug.Log(GIVEUP_STATE.ToString() + "= GIVEUP PLAYBACK STATE");
                    if (GIVEUP_STATE == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                    {
                        robotVoice_GIVEUP.start();
                    }
                    robotVoice_BROKEN.release();
                    robotVoice_HUH.release();
                    robotVoice_AHA.release();
                    break;
                default:
                    break;
            }
        }
        //Invoke("VoicelinePlayed", 2f);
    }

    void VoicelinePlayed()
    {
        playingVoiceline = false;
    }

    private void OnTriggerEnter(Collider collider) // THIS SHOULD MAYBE BE IN OMD SO AS TO NOT BE CALLED SO OFTEN. -c
    {
        if(collider.CompareTag("OMD"))
        {
            RobotBlackboard.OMDBlockReroute(this, collider.GetComponent<OMDWaypoint>().waypointLocation);
        }
    }

    public bool PlayerSeen()
    {
        if(GetSuspicionLevel() >= 0.5f)
        {
            //Debug.Log("PLAYER VIS > 0");
            return (PlayerVisibility() > 0);
        }
        else
        {
            //Debug.Log("PLAYER VIS > 0.4");
            return (PlayerVisibility() > 0.4f);
        }
    }

    void BeginChasingPlayer()
    {
        SetRobotStatus(RobotStates.Chasing);
        AudioHandler.PlayerSeen(true); // <-? Make sure this works!
        anim.SetTrigger("sawPlayer");
        //Debug.Log("Current state is now Robot Chasing (should happen once).");
        PlayVoiceline("event:/s_Robot_AHA");
        RobotBlackboard.robotsChasingYou++;
        //Debug.Log("<color=magenta>-------------------------------------------------</color>" + RobotBlackboard.robotsChasingYou);
        AudioHandler.PlaySound("event:/s_Seen", player.transform.position);
    }

    public void ReturnToWork()
    {
        //Debug.Log("<color=cyan>BACK TO WORK!</color>");
        SetRobotStatus(RobotStates.Walking);
        RobotBlackboard.robotActivities[this] = null;
        currentWaypoint = patrolPositions[currentPatrolPosition];
        PlayVoiceline("event:/s_Robot_GIVEUP");
        busy = false;
    }

    float GetPlayerNoise()
    {
        float result = 0;
        result = player.GetComponent<PlayerMove>().currentVelocity.magnitude; // ?
        if(player.GetComponent<PlayerMove>().raycastDistance < 0.55f) // JACOB SAYS THIS WORKS.
        {
            result /= 2.0f;
        }
        return result;
    }

    void AdjustHearingDistance()
    {
        if(GetSuspicionLevel() < 0.5f)
        {
            if(currentHearingDistance != HEARING_DIST_REG)
            {
                currentHearingDistance = Mathf.Lerp(currentHearingDistance, HEARING_DIST_REG, 0.1f);
            }
            else
            {
                currentHearingDistance = HEARING_DIST_REG;
            }
        }
        else if (GetSuspicionLevel() >= 0.5 && GetSuspicionLevel() < 1f)
        {
            if (currentHearingDistance != HEARING_DIST_MID)
            {
                currentHearingDistance = Mathf.Lerp(currentHearingDistance, HEARING_DIST_MID, 0.5f);
            }
        }
        else
        {
            if (currentHearingDistance != HEARING_DIST_MAX)
            {
                currentHearingDistance = Mathf.Lerp(currentHearingDistance, HEARING_DIST_MAX, 0.75f);
            }
        }
        currentHearingDistance = Mathf.Clamp(currentHearingDistance, HEARING_DIST_REG, HEARING_DIST_MAX);
    }
    // If HeardSomething -> ??? -> Sus.

    // IF PLAYER W/IN HEARING DISTANCE (DEPENDS ON SUS. LVL: 0-.5 = reg, .5-.9 = mid, 1 = max) lerp? 
    //      IF GETPLAYERNOISE < SOME RANDOM NUMBER??????
    //          IF RAYCAST.HIT == PLAYER (AND NOT, LIKE, A BOX)
    //              SUS+=GETPLAYERNOISE...............
    //              if .5-.9 ------> ReportSusActivity
    //              else if >=1 ---> look @ player!
    //              //6lnV7I5qs5O7e1rs3sUA0m?si=3944abaaf1c74f72

    public void CHEAT()
    {
        // DELETE ME AFTER GDEX
        IncreaseSuspicionLevel(1,99f);
        SetLastKnownPlayerDestination(player.transform.position);
        SetRobotStatus(RobotStates.Chasing);
        SetRobotDestination(lastKnownPlayerPosition);
    }

    /*public void AttachToHand(GameObject item)
    {
        item.transform.parent = this.itemHand.transform;
        // reparent the object!
    }*/

    public void NoisemakerPickUp(GameObject item)
    {
        //Debug.Log("yo this works!");
        anim.SetTrigger("pickUp");
        /*
         *          -----------------------------------------
         *          - OKAY, SO THIS CANNOT WORK             -
         *          - "Setting the parent of a transform    -
         *          - which resides in a Prefab Asset is    -
         *          - disabled to prevent data corruption." -
         *          -----------------------------------------
         *          We have to figure out another way.
         * 
         * 
         */
        // item.transform.SetParent(this.itemHand.transform, true);
        // UnityEditor.EditorGUIUtility.PingObject(item.gameObject);
        // Invoke("pickUp", 1.5f); <-- Doesn't work
        // AttachToHand(this.gameObject);
        // anim.ResetTrigger("pickUp");
        //Debug.Log("... well .. at least.... it shooooouuld");
    }

    public void OnNoisemakerPickup()=>this.RobotNoisemaker.RobotNoisemakerAnimationUpdate();
    public void OnNoisemakerThrow()=>this.RobotNoisemaker.RobotDestroysNoisemaker();
    
    public void LerpPOV(float t)
    {
        Color newColor = Color.Lerp(greenlight, Color.red,t);
        POVshaderMaterial.SetColor("EmissionColor", newColor);
    }
}
