using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;

// 2021.11.09 @ 04:16 PM - CB.
// 2022.04.11 @ 07:39 PM - CB.
public class RobotBlackboard : MonoBehaviour
{
    static GameObject                           suspiciousWaypointPrefab;
    static List<GameObject>                     allSabotageWaypoints    = new List<GameObject>(); 
    static public Dictionary<Robot, Waypoint>   robotActivities         = new Dictionary<Robot, Waypoint>();
    static public GameObject[]                  playerRaycastPoints     = new GameObject[5];
    static public List<Robot>                   allRobotsInScene        = new List<Robot>();

    static public int robotsChasingYou = 0;

    private void Awake()
    {
        robotActivities.Clear();
        foreach (Robot r in FindObjectsOfType<Robot>())
        {
            robotActivities.Add(r, null);
        }

        allSabotageWaypoints.Clear();
        foreach (GameObject s in GameObject.FindGameObjectsWithTag("SABO"))
        {
            allSabotageWaypoints.Add(s);
            // turn OFF sabotages here!
        }

        for(int i = 0; i < 5; i++)
        {
            playerRaycastPoints[i] = GameObject.FindGameObjectsWithTag("PlayerRaycastPoint")[i];
            //Debug.Log(playerRaycastPoints[i]);
        }
    }

    public static Robot GetNearestRobot(Vector3 location)
    {
        Robot result = null;
        
        foreach(KeyValuePair<Robot,Waypoint> r in robotActivities)
        {
            Robot current = r.Key;
            result ??= current;

            if(Vector3.Distance(current.transform.position, location) < 
                Vector3.Distance(result.transform.position, location))
            {
                result = current;
            }
        }

        return result;
    }
    
    static public void ReportSuspiciousActivity(Robot robot, Vector3 waypointLocation)
    {
        NavMesh.SamplePosition(waypointLocation, out NavMeshHit hit, 20, NavMesh.AllAreas);
        waypointLocation = new Vector3(waypointLocation.x, hit.position.y, waypointLocation.z);
        //Debug.Log(waypointLocation + ":::" + robot.name);
        
        if (robot != null && waypointLocation.y != Mathf.Infinity && robotActivities.ContainsKey(robot))
        {
            //Debug.Log("<color=red>$$$- REPORT SUS: </color>First if: " + robot.name + " " + waypointLocation + " " + robotActivities.ContainsKey(robot).ToString());
            if (robotActivities[robot] == null)
            {
                //Debug.Log("<color=red>$$$- REPORT SUS: </color>2nd if: " + robotActivities[robot]);
                if (robot.busy)
                {
                    //Debug.Log("<color=red>$$$- REPORT SUS: </color>Robot busy! #1");
                    robot.currentWaypoint.StopCurrentObjective(robot);
                    //robot.busy = false;
                }
                GameObject suspiciousWaypoint = new GameObject("Suspicious Waypoint", typeof(SuspiciousWaypoint));
                suspiciousWaypoint.transform.position = waypointLocation;
                robotActivities[robot] = suspiciousWaypoint.GetComponent<SuspiciousWaypoint>();
                robot.currentWaypoint = robotActivities[robot];
                //Debug.Log("<color=lime>:::" + robot.currentWaypoint.name + " / </color>" + robot.currentWaypoint.transform.position);
                robot.SetRobotDestination(robotActivities[robot].transform.position);
                //Debug.Log("<color=lime>::: Robot -></color>" + robot.destination + "/" + robotActivities[robot].transform.position);
                if (robot.GetSuspicionLevel() < 1)
                {
                    robot.SetRobotStatus(Robot.RobotStates.Searching);
                }
                robot.PlayVoiceline("event:/s_Robot_AHA");
            }
            else
            {
                //Debug.Log("<color=red>$$$- REPORT SUS: </color>First else: Robot has activity: " + robotActivities[robot]);
                Waypoint w = robotActivities[robot];
                if (Vector3.Distance(w.transform.position, waypointLocation) > 1)
                {
                    //Debug.Log("<color=red>$$$- REPORT SUS: </color> Robot is busy #2");
                    robot.currentWaypoint.StopCurrentObjective(robot);
                    /*
                    if (robot.busy)
                    {
                        Debug.Log("<color=red>$$$- REPORT SUS: </color> Robot is busy #2");
                        robot.currentWaypoint.StopCurrentObjective(robot);
                        //robot.busy = false;
                    }
                    */
                    GameObject suspiciousWaypoint = new GameObject("Suspicious Waypoint", typeof(SuspiciousWaypoint));
                    suspiciousWaypoint.transform.position = waypointLocation;
                    robotActivities[robot] = suspiciousWaypoint.GetComponent<SuspiciousWaypoint>();
                    if(w.GetType() == typeof(SuspiciousWaypoint))
                    {
                        //Debug.Log("<color=magenta>THIS IS CORRECT!</color> " + w.GetType());
                        Destroy(w.gameObject);  // Cleans up old waypoints.
                    }
                    robot.SetRobotDestination(waypointLocation);
                    robot.currentWaypoint = robotActivities[robot];
                    if(robot.GetSuspicionLevel() < 1)
                    {
                        robot.SetRobotStatus(Robot.RobotStates.Searching);
                    }
                    //Debug.Log("<color=magenta>UHHH, OK!</color>");
                    robot.PlayVoiceline("event:/s_Robot_AHA");
                }
                else
                {
                    //Debug.Log("<color=red>$$$- REPORT SUS: </color> NEW SUS POINT IS TOO CLOSE!");
                    //Debug.Log("<color=orange>" + Vector3.Distance(w.transform.position, robotActivities[robot].transform.position) + " between " + w.transform.position + " and " + robotActivities[robot].transform.position + "</color>");
                }
            }
        }
        else
        {
            Debug.LogError("Something went wrong with ReportSuspiciousActivity, such as " + robot.name + " or " + waypointLocation + ".");
        }
    }

    public static void OMDBlockReroute(Robot robot, Vector3 waypointLocation)
    {
        NavMesh.SamplePosition(waypointLocation, out NavMeshHit hit, 1, 1);
        waypointLocation = new Vector3(waypointLocation.x, hit.position.y, waypointLocation.z);

         if (robot != null && waypointLocation.y != Mathf.Infinity && robotActivities.ContainsKey(robot))
         {
             if (robotActivities[robot] == null)
            {
                //Debug.Log("<color=red>$$$- REPORT SUS: </color>2nd if: " + robotActivities[robot]);
                if (robot.busy)
                {
                    //Debug.Log("<color=red>$$$- REPORT SUS: </color>Robot busy! #1");
                    robot.currentWaypoint.StopCurrentObjective(robot);
                    //robot.busy = false;
                }
                GameObject OMDWaypoint = new GameObject("OMD Waypoint", typeof(OMDWaypoint));
                OMDWaypoint.transform.position = waypointLocation;
                robotActivities[robot] = OMDWaypoint.GetComponent<OMDWaypoint>();
                robot.currentWaypoint = robotActivities[robot];
                //Debug.Log("<color=lime>:::" + robot.currentWaypoint.name + " / </color>" + robot.currentWaypoint.transform.position);
                robot.SetRobotDestination(robotActivities[robot].transform.position);
                //Debug.Log("<color=lime>::: Robot -></color>" + robot.destination + "/" + robotActivities[robot].transform.position);
                robot.PlayVoiceline("event:/s_Robot_GIVEUP");
            }
            else
            {
                //Debug.Log("<color=red>$$$- REPORT SUS: </color>First else: Robot has activity: " + robotActivities[robot]);
                Waypoint w = robotActivities[robot];
                //Debug.Log("<color=red>$$$- REPORT SUS: </color> Robot is busy #2");
                robot.currentWaypoint.StopCurrentObjective(robot);
                /*
                if (robot.busy)
                {
                    Debug.Log("<color=red>$$$- REPORT SUS: </color> Robot is busy #2");
                    robot.currentWaypoint.StopCurrentObjective(robot);
                    //robot.busy = false;
                }
                */
                GameObject OMDWaypoint = new GameObject("OMD Waypoint", typeof(OMDWaypoint));
                OMDWaypoint.transform.position = waypointLocation;
                robotActivities[robot] = OMDWaypoint.GetComponent<OMDWaypoint>();
                if(w.GetType() == typeof(OMDWaypoint))
                {
                    //Debug.Log("<color=magenta>THIS IS CORRECT!</color> " + w.GetType());
                    Destroy(w.gameObject);  // Cleans up old waypoints.
                }
                robot.SetRobotDestination(waypointLocation);
                robot.currentWaypoint = robotActivities[robot];
                if(robot.GetSuspicionLevel() < 1)
                {
                    robot.SetRobotStatus(Robot.RobotStates.Searching);
                }
                //Debug.Log("<color=magenta>UHHH, OK!</color>");
                robot.PlayVoiceline("event:/s_Robot_GIVEUP");
            }
        }
        else
        {
            Debug.LogError("Something went wrong with ReportSuspiciousActivity, such as " + robot.name + " or " + waypointLocation + ".");
        }
    }

    public static void TurnOnSabotage(SabotageWaypoint waypoint)
    {
        // turn on - must wait for Sabotage prefab!
        Robot nearest = GetNearestRobot(waypoint.transform.position);
         
        if(nearest != null)
        {
            if (nearest.busy)
                {
                    //Debug.Log("<color=red>$$$- REPORT SUS: </color>Robot busy! #1");
                    nearest.currentWaypoint.StopCurrentObjective(nearest);
                    //robot.busy = false;
                }
            robotActivities[nearest] = waypoint;
            Vector3 waypointLocation = waypoint.targetPosition;
            NavMesh.SamplePosition(waypointLocation, out NavMeshHit hit, 1, 1);
            //NEED TO FIX THIS Y VALUE TO ACTUALLY DO A HIT
            waypointLocation = new Vector3(waypointLocation.x, 0.03684608f, waypointLocation.z);
            nearest.currentWaypoint = waypoint;
            nearest.SetRobotDestination(waypointLocation);
            // THIS SHOULD ALWAYS HAPPEN, BUT STILL.
            nearest.SetRobotStatus(Robot.RobotStates.Curious);
            nearest.PlayVoiceline("event:/s_Robot_HUH");
        }
        
        // robot: "Huh?!"
        // robo -> sabo
    }

    bool WaypointLocationAlreadyBeingInvestigated(Vector3 location)
    {
        bool result = false;
        float tooClose = 3f;    // OR WHATEVER.
        foreach(KeyValuePair<Robot, Waypoint> points in robotActivities)
        {
            if(Vector3.Distance(points.Value.transform.position, location) <= tooClose)
            {
                result = true;
            }
        }
        return result;
    }

    public static void CreateNoise(Vector3 noisePosition)
    {
        // PLAYER CALLS THIS WHEN MAKING TOO MUCH NOISE.
        float maxRobotHearingDistance = 10f; // OR WHATEVER IT NEEDS TO BE.
        Robot nearbyRobot = GetNearestRobot(noisePosition);
        if(Vector3.Distance(nearbyRobot.transform.position, noisePosition) <= maxRobotHearingDistance)
        {
            ReportSuspiciousActivity(nearbyRobot, noisePosition);
        }
    }

    public static void UpdateRobotsChasingYou(int i)
    {
        // I'll save this for later! 
        /*
        robotsChasingYou += i;
        if(robotsChasingYou > 1)
        {

        }
        else
        {
            AudioHandler.chaos = 0.
        }*/
    }

    public static void PlayerOnCamera(Vector3 playerPosition)
    {
        // should it be every robot? -c.
        foreach(Robot r in allRobotsInScene)
        {
            r.SetLastKnownPlayerDestination(playerPosition);
            r.suspicionLevel = 1.0f;
            // I THINK THIS WORKS.
        }
    }
}
