﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotAnimationHelper : MonoBehaviour{Robot robot; void Start(){robot ??= GetComponentInParent<Robot>();} public void OnNoisemakerPickup() => robot.RobotNoisemaker.RobotNoisemakerAnimationUpdate();public void OnNoisemakerThrow() => robot.RobotNoisemaker.RobotDestroysNoisemaker();}
// ¯\_(ツ)_/¯ //