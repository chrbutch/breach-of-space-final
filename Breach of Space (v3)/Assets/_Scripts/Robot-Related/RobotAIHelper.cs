using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// 0RleBUunpuPOEINL7MoSOc?si=3c3c62ce4e254644

// 2021.09.19 @ 08:59 PM - CB.
public class RobotAIHelper : MonoBehaviour
{
    public Robot robot;

    public Text sus;
    public Text look;
    public Text see;
    public Text dest;
    public Text busy;
    public Text speed;
    public Text animSpeed;
    public Text animState;
    public Text attn;
    public Text status;
    public Text hear;
    public Text soundRange;

    // Update is called quite a lot, actually.
    void Update()
    {
        SetSuspicion();
        SetLook();
        SetSee();
        SetDest();
        SetBusy();
        SetSpeed();
        SetAnimSpeed();
        animState.text = robot.GetComponentInChildren<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name;
        SetAttention();
        status.text = robot.curState.ToString();
        SetHear();
        SetSoundRange();
    }

    private void SetHear()
    {
        hear.text = "Player Sound: " + robot.PlayerAudibility().ToString();
        if(robot.PlayerAudibility() > 0)
        {
            hear.color = Color.red;
        }
        else
        {
            hear.color = Color.green;
        }
    }

    private void SetSoundRange()
    {
        soundRange.text = "Hearing Range: " + robot.currentHearingDistance.ToString();
        if (robot.currentHearingDistance < 10)
        {
            soundRange.color = Color.green;
        }
        else if(robot.currentHearingDistance < 15 && robot.currentHearingDistance >= 10)
        {
            soundRange.color = Color.yellow;
        }
        else
        {
            soundRange.color = Color.red;
        }
    }

    private void SetDest()
    {
        dest.text = robot.destination.ToString() + " / " + robot.currentWaypoint.transform.position.ToString();
        if(robot.destination.x == robot.currentWaypoint.transform.position.x &&
            robot.destination.z == robot.currentWaypoint.transform.position.z)
        {
            dest.color = Color.green;
        }
        else
        {
            dest.color = Color.red;
        }
    }

    private void SetBusy()
    {
        busy.text = robot.busy.ToString();
        if (robot.busy)
        {
            busy.color = Color.red;
        }
        else
        {
            busy.color = Color.green;
        }

    }

    void SetSuspicion()
    {
        sus.text = "Suspicion Level: " + robot.GetSuspicionLevel().ToString();
        if(robot.GetSuspicionLevel() < 0.5f)
        {
            sus.color = Color.green;
        }
        else if(robot.suspicionLevel < 1 && robot.GetSuspicionLevel() >= 0.5f)
        {
            sus.color = Color.yellow;
        }
        else
        {
            sus.color = Color.red;
        }
    }

    void SetLook()
    {
        if (robot.LookingAtPlayer())
        {
            look.text = "Looking at Player!";
            look.color = Color.green;
        }
        else
        {
            look.text = "Not Looking at Player!";
            look.color = Color.red;
        }
    }

    void SetSee()
    {
        if(robot.PlayerVisibility() <= 0)
        {
            see.color = Color.red;
        }
        else if(robot.PlayerVisibility() > 0 && robot.PlayerVisibility() < 1)
        {
            see.color = Color.yellow;
        }
        else
        {
            see.color = Color.green;
        }
        see.text = "Player Visibility: " + robot.PlayerVisibility().ToString();
    }

    void SetSpeed()
    {
        if(robot.speed <= 0)
        {
            speed.color = Color.red;
        }
        else if(robot.speed > 0 && robot.speed <= 3)
        {
            speed.color = Color.yellow;
        }
        else
        {
            speed.color = Color.green;
        }
        speed.text = "Speed: " + robot.speed.ToString();
    }

    void SetAnimSpeed()
    {
        if (robot.currentSpeed <= 0)
        {
            animSpeed.color = Color.red;
        }
        else if (robot.currentSpeed > 0 && robot.currentSpeed <= 3)
        {
            animSpeed.color = Color.yellow;
        }
        else
        {
            animSpeed.color = Color.green;
        }
        animSpeed.text = "Animation Speed: " + robot.currentSpeed.ToString();
    }

    void SetAttention()
    {
        if(robot.suspicionTimer <= 0)
        {
            attn.color = Color.red;
        }
        else if(robot.suspicionTimer > 0 && robot.suspicionTimer < 5)
        {
            attn.color = Color.yellow;
        }
        else
        {
            attn.color = Color.green;
        }
        attn.text = "Attention: " + robot.suspicionTimer.ToString();
    }
}
