using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using FMODUnity;

public class StartScreen : MonoBehaviour
{
    public StudioEventEmitter soundpiece;

    public void startGame()
    {
        // Start the game button
        SceneManager.LoadScene("GDEX");
        soundpiece.Stop();
    }

    public void openSettings()
    {
        // Open the settings button
    }

    public void quitGame()
    {
        Application.Quit();
        Debug.LogError("Quit the game!!!");
    }
}
