﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EPOOutline;
using TMPro;
//LAST EDITED: 4/21/22 @ 01:11 PM by CB
public class CameraController : MonoBehaviour
{
    public float turnSpeed = 4.0f;
    public float verticalSen = 150f;
    public float horizontalSen = 200;

    private float minTilt = -90f;
    private float maxTilt = 90f;
    private float rotationX = 0f;
    private bool lockCamera = false;

    public Transform player;

    private Vector3 offset;
    private GameObject cam;
    private float raycastDistance = 1.75f;
    public float highlightEasingDuration = 0.2f;
    public GameObject saboItem = null;
    public GameObject resetPop;
    public bool caught = false;

    public TextMeshProUGUI sabo_text_obj;
    private Dictionary<string, string> story_sabo_dict = new Dictionary<string, string>();

    void Start()
    {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		offset = new Vector3(player.position.x, player.position.y + 3.0f, player.position.z + 4.0f);
        cam = gameObject;

        // Initialize sabotagable story boards
        story_sabo_dict.Clear();
        story_sabo_dict.Add("A", "A msg here");
		story_sabo_dict.Add("B", "B msg here");
		story_sabo_dict.Add("C", "C msg here");
		story_sabo_dict.Add("D", "D msg here");
		story_sabo_dict.Add("E", "E msg here");                                                        
    }

    void LateUpdate()
    {
        //disabled pause menu for now.
        /*if (PauseMenu.GameIsPaused == false)
        {
            offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
            transform.position = player.position + offset;
            transform.LookAt(player.position);
        }*/
    }

    public void flipLock()
    {
        lockCamera = !lockCamera;
    }

    private void Update()
    {
        if (caught)
        {
            //print("YOU BEEN CAUGHT");
            /*Robot pickupRobot = RobotBlackboard.GetNearestRobot(cam.transform.position);
            float targetY = pickupRobot.transform.rotation.y+180;
            float targetX = pickupRobot.lossCameraHelper.transform.position.x;
            float targetYl = pickupRobot.lossCameraHelper.transform.position.y;
            float targetZ = pickupRobot.lossCameraHelper.transform.position.z;
            float xr = 0f;
            float yr = 0f;
            float zr = 0f;
            float xp = 0f;
            float yp = 0f;
            float zp = 0f;
            if (cam.transform.rotation.x != 0)
            {
               if (cam.transform.rotation.x > 0)
               {
                   xr = -5f * Time.deltaTime;
                   if(cam.transform.rotation.x + xr < 0)
                   {
                       xr = 0 - cam.transform.rotation.x;
                   }
               }
               else if (cam.transform.rotation.x < 0)
               {
                   xr = 5f * Time.deltaTime;
                   if(cam.transform.rotation.x + xr > 0)
                   {
                       xr = 0 - cam.transform.rotation.x;
                   }
               }
            }
            if (cam.transform.rotation.y != targetY)
            {
               if (cam.transform.rotation.y > targetY)
               {
                   yr = -5f * Time.deltaTime;
                   if(cam.transform.rotation.y + yr < targetY)
                   {
                       yr = targetY - cam.transform.rotation.y;
                   }
               }
               else if (cam.transform.rotation.y < targetY)
               {
                   yr = 5f * Time.deltaTime;
                   if(cam.transform.rotation.y + yr > targetY)
                   {
                       yr = targetY - cam.transform.rotation.y;
                   }
               }
            }
            if (cam.transform.rotation.z != 0)
            {
               if (cam.transform.rotation.z > 0)
               {
                   zr = -5f * Time.deltaTime;
                   if(cam.transform.rotation.z + zr < 0)
                   {
                       zr = 0 - cam.transform.rotation.z;
                   }
               }
               else if (cam.transform.rotation.z < 0)
               {
                   zr = 5f * Time.deltaTime;
                   if(cam.transform.rotation.z + zr > 0)
                   {
                       zr = 0 - cam.transform.rotation.z;
                   }
               }
            }
            cam.transform.rotation = Quaternion.Euler((cam.transform.rotation.x + xr),180,(cam.transform.rotation.z + zr));
            //location ----------------------------------------
            if (cam.transform.position.x != targetX)
            {
               if (cam.transform.position.x > targetX)
               {
                   xp = -5f * Time.deltaTime;
                   if(cam.transform.position.x + xp < targetX)
                   {
                       xp = targetX - cam.transform.position.x;
                   }
               }
               else if (cam.transform.position.x < targetX)
               {
                   xp = 5f * Time.deltaTime;
                   if(cam.transform.position.x + xp > targetX)
                   {
                       xp = targetX - cam.transform.position.x;
                   }
               }
            }
            if (cam.transform.position.y != targetYl)
            {
               if (cam.transform.position.y > targetYl)
               {
                   yp = -5f * Time.deltaTime;
                   if(cam.transform.position.y + yp < targetYl)
                   {
                       yp = targetYl - cam.transform.position.y;
                   }
               }
               else if (cam.transform.position.y < targetYl)
               {
                   yp = 5f * Time.deltaTime;
                   if(cam.transform.position.y + yp > targetYl)
                   {
                       yp = targetYl - cam.transform.position.y;
                   }
               }
            }
            if (cam.transform.position.z != targetZ)
            {
               if (cam.transform.position.z > targetZ)
               {
                   zp = -5f * Time.deltaTime;
                   if(cam.transform.position.z + zp < targetZ)
                   {
                       zp = targetZ - cam.transform.position.z;
                   }
               }
               else if (cam.transform.position.z < targetZ)
               {
                   zp = 5f * Time.deltaTime;
                   if(cam.transform.position.z + zp > targetZ)
                   {
                       zp = targetZ - cam.transform.position.z;
                   }
               }
            }
            cam.transform.position += new Vector3(xp,yp,zp);*/
        }
        else if (!lockCamera)
        {
            // Player camera tilt (look up and down)
            float mouseX = Input.GetAxis("Mouse X") * horizontalSen * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * verticalSen * Time.deltaTime;

            rotationX -= mouseY;
            rotationX = Mathf.Clamp(rotationX, minTilt, maxTilt);

            transform.localRotation = Quaternion.Euler(rotationX, 0f, 0f);
            player.Rotate(Vector3.up * mouseX);

            RaycastHit hit;
            int mask = 1 << LayerMask.NameToLayer("Default");

            //Raycast downwards
            Debug.DrawRay(transform.position,transform.forward,Color.cyan);
            if (Physics.Raycast(transform.position, transform.forward, out hit,
                raycastDistance, ~mask))
            {
                // Check if we have old saboItems
                if(hit.collider.gameObject.layer == 7)
                {
                    saboItem = hit.collider.gameObject;
                    HighlightSabotagable(saboItem);
                }
                else
                {
                    DeHighlightSabotageable(saboItem);
                    if (saboItem != null)
                        saboItem = null;
                }
                // Highlight saboItem
                
            }
            else
            {
                // De-Highlight saboItem
                DeHighlightSabotageable(saboItem);
                saboItem = null;
            }
        }
    }

    public IEnumerator startHighlightEasing(GameObject sabotagable, Color fromColor, Color toColor)
    {
        Color color = fromColor;
        float fromAlpha = fromColor.a;
        float toAlpha = toColor.a;

        float timeElapsed = 0f;
        var outlineableObject = sabotagable.GetComponent<Outlinable>();

        while (timeElapsed < highlightEasingDuration)
        {
            float t = timeElapsed / highlightEasingDuration;
            t = t * t * (3f - 2f * t);

            // Set the alpha of the sabotagable color
            color.a = Mathf.Lerp(fromAlpha, toAlpha, t);
            outlineableObject.OutlineParameters.FillPass.SetColor("_PublicColor", color);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        outlineableObject.OutlineParameters.FillPass.SetColor("_PublicColor", color);

        if (toAlpha == 0f) 
            outlineableObject.OutlineParameters.Enabled = false;
    }

    public void HighlightSabotagable(GameObject sabotagable)
    {
        // Show sabotagable text if the name matches
        // TODO: NEED TO CHANGE THIS
        if (sabotagable.name.ToLower().Contains("story"))
        {                                                                   // Mon Apr 18, 11:52PM, Tianning
            //Debug.Log("$$$$$$$1" + sabotagable.name);                                    // Reached this line, sabotagable is not null, prints the name fine
            //Debug.Log("$$$$$$1.5: " + sabotagable.name.Split(' ')[2]);
            string key = sabotagable.name.Split(' ')[2];   // Reached this line, sabotagable is null, NO IDEA WHY - FIXED by never
            //Debug.Log("$$$$$$$2" + story_sabo_dict[key]);                                // Meaningless code since sabotagable is null
            sabo_text_obj.text = story_sabo_dict[key];                      // Meaningless code since sabotagable is null
        }

        // NOTE FROM CB (4/21/22):
        //  The below code is handled in another script now
        //  (specifically OutlinePlayerInteraction.cs), so
        //  it is unnecessary to be called here.
        //
        //var outlineableObject = saboItem.GetComponent<Outlinable>();
        //outlineableObject.OutlineParameters.Enabled = true;

        //Color fromColor = outlineableObject.OutlineParameters.FillPass.GetColor("_PublicColor");
        //Color toColor = fromColor;
        //toColor.a = 0.4f;

        //StartCoroutine(startHighlightEasing(saboItem, fromColor, toColor));
    }

    public void DeHighlightSabotageable(GameObject sabotagable)
    {
        if (sabotagable != null)
        {
            // Hide sabotagable text if the name matches
            if (sabotagable.name.ToLower().Contains("story"))
            {
                sabo_text_obj.text = "";
            }

            //var outlineableObject = saboItem.GetComponent<Outlinable>();

            //Color fromColor = outlineableObject.OutlineParameters.FillPass.GetColor("_PublicColor");
            //Color toColor = fromColor;
            //toColor.a = 0f;

            //StartCoroutine(startHighlightEasing(saboItem, fromColor, toColor));
        }
    }

    public GameObject getsaboItem()
    {
        return saboItem;
    }

    public void gotCaught(GameObject robo)
    {
        //cam.transform.parent = robo.transform; //<------------------ throwing an NullRefEx.??? -cb
        caught = true;
        StartCoroutine(resetPopup(4f));
    }

    IEnumerator resetPopup(float time)
    {
        yield return new WaitForSeconds(time);
        resetPop.SetActive(true);
    }
}
