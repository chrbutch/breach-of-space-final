using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextContainer
{
    public List<TextData> texts;
}

[System.Serializable]
public class TextData
{
    public int textID;
    public string textLabel;
    public string textBody;
}




