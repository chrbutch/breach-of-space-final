using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

[RequireComponent(typeof(MeshRenderer))]
// 2022.02.16 @ 07:40 PM - CB.
public class SecurityCamera : MonoBehaviour
{
    [SerializeField]
    private Color _alertColor = Color.red;
    private Animator _anim = null;
    private MeshRenderer _renderer = null;


   // public GameObject robot;
    public GameObject player;

   Vector3 playerPosition;

    public FMODUnity.StudioEventEmitter emitter;

    Color greenlight = new Color((float)(8 / 255), (float)(64 / 255), 0);

    // Start is called before the first frame update
    private void Start()
    {


        _renderer = GetComponent<MeshRenderer>();

        if (transform.parent != null)
        {
            _anim = GetComponentInParent<Animator>();
            if (_anim == null)
            {
                Debug.LogError("Animator is Null"); // <-----------?????????????????
            }
        }
    }

    /// If player is caught in the light then the light will turn red 
    /// add alarm sound here 
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            // Robot will go to the camera/player's position 
            RobotBlackboard.PlayerOnCamera(playerPosition);

            /// light will change to red 
            _renderer.material.SetColor("EmissionColor", _alertColor);

            if (!emitter.IsPlaying()) // So it doesn't keep trying to start playing.
            {
                emitter.SetParameter("Off", 0);
                emitter.Play();
            }
            
            // when player is gone/alarm ends: emitter.SetParameter("Off", 1f);


            if (_anim != null)
            {
                _anim.enabled = false;
            }


        }
    }

    private void OnTriggerExit(Collider other)
    {
        // when player is gone/alarm ends
        emitter.SetParameter("Off", 1f);
        // Could maybe do a while loop here to Lerp between Red & Green (might be nice)?
        _renderer.material.SetColor("EmissionColor", greenlight);
    }
}

 