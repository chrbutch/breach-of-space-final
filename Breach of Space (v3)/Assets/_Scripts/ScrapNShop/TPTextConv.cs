using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TPTextConv : MonoBehaviour
{
    public Text tpText;
    public NoisePlayer pm;

    void Start()
    {
        // tpText = GetComponent<Text>();
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        tpText.text = pm.throwPower.ToString();
    }
}
