using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bolt", menuName = "Scrap/Bolt")]
public class Bolt : Scrap
{
    public int numOfBolts;
    // public pileBolts pile;

    

    public override void PickUp()
    {
        base.PickUp();

    }

    private void OnDisable()
    {
        Debug.Log("Bolt disabled");
        //pile.setDialogue();
    }
}