using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScrapCraft : MonoBehaviour
{
    public PlayerMove play;
    public DialogueTxt dTxt;
    public int playBolt;
    //public Toggle boltTogg;
    //public int playNut;
    //public int playMet;

    public bool itemInUse = false;
    private int noisemakerCost = 3;
    private int craftable2Cost = 9;
    private int craftable3Cost = 27;
    //public InputField boltPay;
    //public InputField nutPay;
    //public InputField metPay;


    void Update()
    {
        // playBolt = play.playerBolt;
        //playNut = play.playerNuts;
        //playMet = play.playerMetal;
    }

    //The below function is depreciated
    /*public void TurnBoltToggleOff()
    {
        boltTogg.isOn = false;
    }*/


    public void DoCraft()
    {
        /*if (itemInUse == false)
        {
            if (0 < noisemakerCost && noisemakerCost <= play.playerBolt)
            {
                // play.playerBolt = play.playerBolt - noisemakerCost;
                CraftNoisemaker();
                //TurnBoltToggleOff();
                Debug.LogWarning("There are " + (playBolt - noisemakerCost) + " bolts.");
            }
            else if (0 >= noisemakerCost || noisemakerCost > play.playerBolt)
            {
                //TurnBoltToggleOff();
                dTxt.txt.text = "You must collect more scrap first!";
                Debug.LogWarning("You need more scrap first!");
            }
        }
        else if (itemInUse == true)
        {
            dTxt.txt.text = "You already have an item in hand!";
            Debug.LogWarning("You already have an item in hand");
        }*/
    }

    public void UndoCraft()
    {
        if (itemInUse == true)
        {
            // play.playerBolt = play.playerBolt + noisemakerCost;
            ScrapNoisemaker();
            Debug.LogWarning("There are " + (playBolt + noisemakerCost) + " bolts.");
        }
        else if (itemInUse == false)
        {
            dTxt.txt.text = "You don't have any items to scrap!";
            Debug.LogWarning("You don't have any items to scrap!");
        }
    }

    public void CraftNoisemaker()
    {
        // Adds the noisemaker to the player inventory
        itemInUse = true;
        dTxt.txt.text = "The Noisemaker has been crafted!";
        Debug.LogWarning("Noisemaker added to your inventory");
    }

    public void ScrapNoisemaker()
    {
        // Returns player bolts for the noisemaker
        itemInUse = false;
        dTxt.txt.text = "The Noisemaker has been scraped!";
        Debug.LogWarning("Your scraps have been returned");
    }

}
