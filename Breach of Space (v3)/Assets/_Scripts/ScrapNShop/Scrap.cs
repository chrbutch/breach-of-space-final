using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scrap", menuName = "Scrap")]
public class Scrap : ScriptableObject
{
    // new public string name = "Scrap";
    public Sprite icon = null;
    public bool lowVal = false;
    public bool medVal = false;
    public bool highVal = false;

    public virtual void PickUp()
    {


        Debug.Log("Attached to: " + name);
    }


}