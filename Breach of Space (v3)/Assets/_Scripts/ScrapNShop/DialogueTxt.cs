using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTxt : MonoBehaviour
{
    public Text txt;
    private PlayerMove pm;
    public GameObject scrapMenu;
    public GameObject craftMenu;
    /*private PileBolts pileB = null;
    private PileNuts pileN = null;
    private PileMetel pileM = null;*/

    void Start()
    {
        txt = GetComponent<Text>();
        pm = GetComponent<PlayerMove>();
        ClearTxt();
    }

    private void Update()
    {
        
        /*if (scrapMenu.activeInHierarchy || craftMenu.activeInHierarchy)
        {
            ClearTxt();
        }*/
        //----------------------------------------------------------------------------------
        /*if (pm.nearBolt != null && pm.nearBolt.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearBolt.startCoro = false;
        }
        if (pm.nearNut != null && pm.nearNut.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearNut.startCoro = false;
        }
        if (pm.nearMetal != null && pm.nearMetal.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearMetal.startCoro = false;
        }*/

    }

    public void TriggerScrap(PileBolts pileB = null/*, PileNuts pileN = null, PileMetel pileM = null*/)
    {/*
        if (pm.nearBolt != null && pm.nearBolt.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearBolt.startCoro = false;
        }
        
        if (pm.nearNut != null && pm.nearNut.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearNut.startCoro = false;
        }
        if (pm.nearMetal != null && pm.nearMetal.startCoro)
        {
            StartCoroutine(ClearDialogue(3));
            pm.nearMetal.startCoro = false;
        }*/
    }

    public void ClearTxt()
    {
        Debug.Log("Clear txt start");
        txt.text = "";
    }

    IEnumerator ClearDialogue(float waitTime)
    {
        Debug.Log("Coroutine Started!");
        yield return new WaitForSeconds(waitTime);
        ClearTxt();
        Debug.Log("Coro End");
        yield return null;
    }
}
