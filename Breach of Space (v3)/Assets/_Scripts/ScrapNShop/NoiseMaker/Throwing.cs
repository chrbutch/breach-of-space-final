using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwing : MonoBehaviour
{
    /// <summary>

    /// WARNING: DEPRICIATED 

    /// This script will simulate the throw for the noisemaker. It will determine the direction, power, 
    /// and arc of the throw.
    /// 
    /// s = displacement, u = intial velocity, v = final velocity, a = acceleration, t = time
    /// v = u + at, s = ((u + v) / 2)t, s = ut + ((at^2) / 2), s = vt - (at^2 / 2), v^2 = u^2 + 2as
    /// speed = distance / time
    /// </summary>
    
    public Rigidbody noiser;
    public GameObject noiseGame;
    public Transform targetT;
    Vector3 FindThrowDistance;


    public float arcHeight = 25;
    public float gravity = -18;
    public float holdTime = 0f; // The amount of time the spacebar is held down
    public int throwDist = 0;
    public bool throwBuild = false;
    public bool isParented = true;


    void Start()
    {
        noiser.useGravity = false;
        // noiseGame.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            holdTime = 0f;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            // Move the transform farther forward
            throwBuild = true;
            holdTime += Time.deltaTime;
            // noiseGame.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            throwBuild = false;
            Launch();
            Debug.LogWarning(holdTime);
            Debug.LogWarning(throwDist);
        }// WE SHOULD FIX THIS - C.
        if (noiser.transform.parent == null)
        {
            isParented = false;
        }

        if (throwBuild == true)
        {
            // Changes the case number that adjusts the velocity of a throw
            if (holdTime <= 1f)
            {
                throwDist = 1;
                FindThrowDistance = new Vector3(1, 1, 0.25f);
            }
            else if (holdTime > 1f && holdTime < 3f)
            {
                throwDist = 2;
                FindThrowDistance = new Vector3(1, 1, 0.50f);
            }
            else if (holdTime >= 3f)
            {
                throwDist = 3;
                FindThrowDistance = new Vector3(1, 1, 0.75f);
            }
        }
    }

    /*Vector3 FindThrowDistance()
    {
        switch (throwDist)
        {
            case 3:
                return new Vector3(1, 1, 0.75f);
                break;
            case 2:
                return new Vector3(1, 1, 0.50f);
                break;
            case 1:
                return new Vector3(1, 1, 0.25f);
                break;
            default:
                return new Vector3(1, 1, 0.50f);
                break;
        }
    }*/

    /*Vector3 SmallThrow() 
    {
        return new Vector3(1, 1, 0.25f);
    }

    Vector3 MediumThrow() 
    {
        return new Vector3(1, 1, 0.50f);
    }

    Vector3 LargeThrow() 
    {
        return new Vector3(1, 1, 0.75f);
    }*/

    void Launch()
    {
        if (isParented == true)
        {
            Physics.gravity = Vector3.up * gravity;
            noiser.useGravity = true;
            noiser.velocity = Vector3.Scale(FindLaunchVelocity(), FindThrowDistance);
            Debug.LogWarning(FindLaunchVelocity());
            Debug.LogWarning(noiser.transform.position);
            if (noiser.useGravity == true)
            {
                noiser.transform.parent = null;
            }
        }
    }

    Vector3 FindLaunchVelocity()
    {
        float displaceY = targetT.position.y - noiser.position.y;
        Vector3 displaceXZ = new Vector3(targetT.position.x - noiser.position.x, 0, targetT.position.z - noiser.position.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * arcHeight);
        Vector3 velocityXZ = displaceXZ / (Mathf.Sqrt(-2*arcHeight/gravity) + Mathf.Sqrt(2*(displaceY - arcHeight)/gravity));

        return velocityXZ + velocityY;
    }
    
}
