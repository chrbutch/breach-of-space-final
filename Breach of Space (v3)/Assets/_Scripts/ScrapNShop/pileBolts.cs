using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PileBolts : MonoBehaviour
{
    public Bolt piles;
    public PlayerMove pm;
    public DialogueTxt dTxt;
    public Text txxt;
    public Image boltPic;
    public int boltNum;
    //public int boltVal;
    public bool startCoro = false;
    public GameObject scrapMenu;
    //public GameObject openScrap;

    void Start()
    {
        txxt = txxt.GetComponent<Text>();
        txxt.text = "???";
        boltPic = boltPic.GetComponent<Image>();
        
        scrapMenu.SetActive(false);
        //openScrap.SetActive(true);

        /*if (piles.lowVal == true)
        {
            boltVal = 1;
        }
        else if (piles.medVal == true)
        {
            boltVal = 2;
        }
        else if (piles.highVal == true)
        {
            boltVal = 3;
        }
        else
        {
            Debug.LogWarning("The scarp has an invalid value");
        }*/

    }

    private void ResetTxt()
    {
        boltPic.sprite = piles.icon;
        // txxt.text = "Bolts: " + pm.playerBolt.ToString();

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            // pm.playerBolt += boltNum;
            ResetTxt();
            dTxt.txt.text = "You picked up " + boltNum.ToString() + " bolts.";
            startCoro = true;
            this.gameObject.SetActive(false);

        }

    }

}
