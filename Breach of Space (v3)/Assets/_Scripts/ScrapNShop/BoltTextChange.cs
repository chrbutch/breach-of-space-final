using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoltTextChange : MonoBehaviour
{
    public Text text;
    int playBoltConvert;
    public PlayerMove pm;

    void Start()
    {
        text = GetComponent<Text>();
        //pm = GetComponent<PlayerMove>();
    }

    // Update is called once per frame
    void Update()
    {
        // playBoltConvert = pm.playerBolt;
        text.text = playBoltConvert.ToString();
    }
}
