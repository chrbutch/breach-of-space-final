using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loaded Area", menuName = "ScriptableObjects/Loaded Area")]
public class LoadedArea : ScriptableObject
{
    public short areaNumber;
    public Vector3 playerLoadPosition;
    public Vector3 playerLoadRotation;
    public string songTitle;
    public GameObject[] turnedOffSabotagables;

    GameObject player;

    public void OnLoad()
    {
        player = FindObjectOfType<PlayerMove>().gameObject;
        player.GetComponent<ProgressionHelper>().OpenAreas(areaNumber);

        player.transform.position = playerLoadPosition;
        player.transform.rotation = Quaternion.Euler(playerLoadRotation);

        if(areaNumber != 0)
        {
            AudioHandler.ChangeSong(songTitle);
        }

        CutsceneHelper.sceneNumber = areaNumber;
        CutsceneHelper.ready = true;
    }
}
