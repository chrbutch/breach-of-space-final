using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Icon_ScrObj", menuName = "ScriptableObjects/Icon")]
public class Iconography : ScriptableObject
{
    public Sprite icon_Robot;
    public Sprite icon_Story;
    public Sprite icon_Sabotage;
    public Sprite icon_Forcefield;
    public Sprite icon_Scrap;
}
