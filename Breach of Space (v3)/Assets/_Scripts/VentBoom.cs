using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentBoom : MonoBehaviour
{
    ///
    /// The purpose of this script is to open the vent as well as
    /// activate the particle effect that simulates an explosion.
    ///

    public bool hasExploded = false;

    public GameObject explosionEffect;
    Animation anim;
    
    void Start()
    {
        anim = gameObject.GetComponent<Animation>();
    }

    void Update()
    {/*
        if (Input.GetKeyDown("v"))
        {
            Explode();
        }*/ 
    }

    public void Explode() 
    {
        if (hasExploded == false)
        {
            AudioHandler.PlaySound("event:/s_MaintShaft_Vent", this.transform.position);
            //Debug.LogWarning("BOOOOOM!");
            Instantiate(explosionEffect, transform.position, transform.rotation);
            hasExploded = true;
            anim.Play("VentOpenAnim");
        }
        else
        {
            //Debug.LogWarning("... already went boom");
        }
    }
}
