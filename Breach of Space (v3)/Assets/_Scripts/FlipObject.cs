using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipObject : MonoBehaviour
{
    public bool isFlipped = false;
    FMODUnity.StudioEventEmitter emitter;

    private void Start()
    {
        TryGetComponent<FMODUnity.StudioEventEmitter>(out emitter);
        //emitter = GetComponent<FMODUnity.StudioEventEmitter>();

        if(emitter != null && this.gameObject.activeInHierarchy){
            emitter.Play();
        }

        //if (this.gameObject.activeInHierarchy)
        //{
        //    emitter.Play();
        //}
    }

    public void Flip()
    {
        float audioOff = (!isFlipped) ? 1.0f : 0.0f;
        if (emitter != null)
        {
            emitter.SetParameter("Off", audioOff);
        }
        // Flip for ForceField
        if (gameObject.tag == "ForceField")
        {
            gameObject.SetActive(isFlipped);
        }

        // Flip for security camera
        if (gameObject.tag == "CameraCone")
        {
            gameObject.SetActive(isFlipped);
        }

        // Flip for OMD
        if (gameObject.tag == "OMDParent")
        {
            foreach(Transform child in transform)
            {
                child.gameObject.GetComponent<FlipObject>().Flip();
            }
        }

        if(gameObject.tag == "OMD")
        {
            gameObject.SetActive(isFlipped);
        }

        // Flip it
        isFlipped = !isFlipped;
    }
}
