using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Christopher Butcher - 4/11/22 @ 7:16 PM
[RequireComponent(typeof(EPOOutline.Outlinable))]
public class OutlinePlayerInteraction : MonoBehaviour
{
    static GameObject player;       // The player GameObject: used to determine location.
    EPOOutline.Outlinable outline;  // The Outlinable component attached to this object.
    // The colors of the object (used for Color.Lerp)
    Color outlineMaxColor, outlineOffColor, fillMaxColor, fillOffColor, iconMaxColor;     
    bool inRange;                   // Boolean to determine if player is nearby.

    // These determine all outlinables.
    const float OUTLINE_ALPHA_MAX = 1.0f, FILL_ALPHA_MAX = 0.6f;

    [Header("Max Visibile Distance to Player")]
    public float outlineDistance = 15f;
    public float fillDistance    = 10f;

    public enum TypeOfIcon { Robot, Sabotage, StorySabotage, Forcefield, Scrap };

    [Header("Icon")]
    public TypeOfIcon iconType;
    public Iconography scrObj;
    SpriteRenderer icon;
    GameObject iconObject;

    Camera mainCam;

    void Start()
    {
        player  = GameObject.FindWithTag("Player");        // Get player (once)
        outline = GetComponent<EPOOutline.Outlinable>();
        inRange = false;

        // Create color (based on parameters)
        outlineMaxColor = new Color(
            outline.OutlineParameters.Color.r,
            outline.OutlineParameters.Color.g,
            outline.OutlineParameters.Color.b, OUTLINE_ALPHA_MAX);
        outlineOffColor = new Color(
            outline.OutlineParameters.Color.r,
            outline.OutlineParameters.Color.g,
            outline.OutlineParameters.Color.b, 0);
        fillMaxColor    = new Color(
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").r,
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").g,
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").b, FILL_ALPHA_MAX);
        fillOffColor    = new Color(
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").r,
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").g,
            outline.OutlineParameters.FillPass.GetColor("_PublicColor").b, 0);

        // Assign & adjust the icon
        iconObject = AssignIcon();

        // Position the icon
        float newHeight = (this.gameObject.transform.localScale.y / 2) + (this.gameObject.transform.localScale.y / 8) + 1.5f;
        iconObject.transform.localPosition += new Vector3(0, newHeight, 0);
        iconObject.transform.localScale /= 2;

        // Sets outline alpha to 0 rather than turning off component.
        TurnOffOutliner();

        // Find camera (for LookAt)
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (inRange)
        {
            LerpDistanceValues();

            if (mainCam != null)
            {
                iconObject.transform.LookAt(mainCam.transform.position);
            }
            
        }
    }
    
    #region OnTrigger...
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (!inRange)
            {
                inRange = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !inRange)
        {
            inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
            TurnOffOutliner();
        }
    }
    #endregion

    /// <summary>
    /// Updates the color based on
    /// the distance between the 
    /// Outlinable's 'off' color &
    /// its 'on color.
    /// </summary>
    void LerpDistanceValues()
    {
        if(player != null)
        {
            Vector2 newValues = EvaluateNormalizedDistance(player.transform.position);
            //Debug.Log("///<color=magenta>LerpDistanceValues</color><color=cyan> New Values!</color>: " + newValues.x + ", " + newValues.y);
            Color newOutlineColor = Color.Lerp(outlineOffColor, outlineMaxColor, newValues.x);
            Color newFillColor    = Color.Lerp(fillOffColor,    fillMaxColor,    newValues.y);
            Color newIconColor    = Color.Lerp(outlineOffColor, iconMaxColor,    newValues.x);
            //Debug.Log(newOutlineColor + " /// " + newFillColor);
            icon.color = newIconColor;
            outline.OutlineParameters.Color = newOutlineColor;
            outline.OutlineParameters.FillPass.SetColor("_PublicColor", newFillColor);
        }
        else
        {
            Debug.LogError("<color=cyan>PLAYER IS NULL</color>");
        }
    }

    /// <summary>
    /// Calculates the normalized distance from the player
    /// </summary>
    /// <param name="playerPosition">The Player GameObject's position</param>
    /// <returns></returns>
    Vector2 EvaluateNormalizedDistance(Vector3 playerPosition)
    {
        float distanceToPlayer = Vector3.Distance(this.transform.position, playerPosition);
        float normalizedOutlineDistance = 1 - (distanceToPlayer / outlineDistance);
        float normalizedFillDistance    = 1 - (distanceToPlayer / fillDistance   );
        return new Vector2(Mathf.Clamp01(normalizedOutlineDistance), Mathf.Clamp01(normalizedFillDistance));
    }

    /// <summary>
    /// Returns the Outlinable's color to
    /// its 'off' color.
    /// </summary>
    void TurnOffOutliner()
    {
        //Debug.Log("///Turned off Outliner for " + this.gameObject.name);
        outline.OutlineParameters.Color = outlineOffColor;
        outline.OutlineParameters.FillPass.SetColor("_PublicColor", fillOffColor);
        icon.color = fillOffColor;
    }

    private GameObject AssignIcon()
    {
        GameObject result = new GameObject("ICON!");
        result.transform.position = this.transform.position;
        result.transform.SetParent(this.transform);
        result.AddComponent<SpriteRenderer>();
        icon = result.GetComponent<SpriteRenderer>();
        switch (iconType)
        {
            case TypeOfIcon.Robot:
                icon.sprite = scrObj.icon_Robot;
                break;
            case TypeOfIcon.Scrap:
                icon.sprite = scrObj.icon_Scrap;
                break;
            case TypeOfIcon.StorySabotage:
                icon.sprite = scrObj.icon_Story;
                break;
            case TypeOfIcon.Sabotage:
                icon.sprite = scrObj.icon_Sabotage;
                break;
            case TypeOfIcon.Forcefield:
                icon.sprite = scrObj.icon_Forcefield;
                break;
            default:
                Debug.LogWarning("<color=cyan>" + this.gameObject.name + " has not been assigned an icon!</color>");
                break;
        }
        icon.color = new Color(outlineMaxColor.r, outlineMaxColor.g, outlineMaxColor.b, 1.0f);
        iconMaxColor = icon.color;
        return result;
    }
}
