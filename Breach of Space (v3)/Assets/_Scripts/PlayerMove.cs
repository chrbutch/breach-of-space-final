using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using Cinemachine;
using FMODUnity;
using UnityEngine.UI;

// 2021.11.07 @ 03:56 PM - CB.
public class PlayerMove : MonoBehaviour
{
    /* Rigidbody Player movement variables */
    private float playerSpeed = 40f;
    private float horizontalMovement;
    private float verticalMovement;
    private float leftShift;
    private Vector3 moveDirection;
    private Rigidbody playerRigidBody;
    /* =================================== */

    public float easingDuration = 0.3f; //eases FOV changes
    public bool isChasing = false; //whether or not any robot is chasing the player
    private bool callOnceForVignette = false;
    private bool isOnStairs = false;
    private const float ogFOV = 60f;
    private const float toFOV = 80f;
    public GameObject vignetteEffect;
    public GameObject CMvcam1;

    //public CharacterController controller;

    public Transform cam; //the camera's Transform
    public CameraController camController;
    public Transform playerBody; //the transform of the capsule the represents the player

    private FMODUnity.StudioEventEmitter footstepEmitter;
    
    Vector3 velocity; //player's velocity

    private float mass = 3.0f; //player's mass, used for calculations in movement
    private static float mouseSen = 200f;

    private float playerJumpForce = 170f;
    public float gravity = -9.8f;

    public float raycastDistance = 0.56f; //distance of the raycast to check if player is touching the ground.
    private float capRaycastDistance = 0.55f; //distance of the raycast to check if the player can uncrouch.

    public bool active = true; //what is this? -JS
    public bool addTex = false; //what is this? -JS
    public bool checking = false; //what is this? -JS

    //private float groundedCooldown = 4;

    public EndUI end;
    public GameObject mouseIcon; //Popup used to show the player can click on a given portion
    public NoisemakerWaypoint noiseWay; // Retrieves the OnNoisemakerStart() and OnNoisemakerEnd() functions from NoisemakerWaypoint
    public NoisePlayer Noplay;
    public bool stopHidingMouseIcon = false; // when true, displays the "click" icon

    public GameObject thePauseMenu;

    public DialogueTxt dTxxt; // This is text informs the player when something happens

    // public PileBolts nearBolt = null; // This detects the nearest scrap in order to craft the noisemaker
    //[Header("Crouching")]
    private Vector3 CrouchScale = new Vector3(0.47f, 0.15f, 0.47f); // the scale ratio for the player when they're crouching
    private Vector3 CrouchCorrection = new Vector3(0f, 0.4f, 0f); //the force applied to the player to make sure they don't clip into the ground when they uncrouch.
    private Vector3 FullScale = new Vector3(0.47f, 0.47f, 0.47f); //the scale ratio for the player when they're not crouching.
    public Vector3 currentVelocity = Vector3.zero; //velocity of the player used in adjustment of player calculation.
    public Vector3 forcefieldCollisionContactPoint;
    // private Vector3 impact = Vector3.zero; //@MarticZtn can you describe this? -JS
    // public Robot rob; <-- Used for debugging

    public alphaSpammer fadeOut; //a way to fade out the player's vision by slowly loading in a black image.
    public static bool gameOver = false; //set to true when player game overs
    public static bool pausePlayer = true;
    private bool inMinigame = false; //tracks whether or not player is in sabotage minigame
    private int movementState = 0;  //  0:  IDLE
                                    //  1:  CROUCHING
                                    //  2:  WALKING
                                    //  3:  RUNNING
    PlayerData pData;

    private void Start()
    {
        // Disable V-Sync
        QualitySettings.vSyncCount = 0;

        // Cap the target framerate at 60fps
        Application.targetFrameRate = 60;
        
        playerRigidBody = GetComponent<Rigidbody>();

        footstepEmitter = GetComponent<FMODUnity.StudioEventEmitter>();
        footstepEmitter.Play();
        footstepEmitter.SetParameter("MovementState", 0);
        //thePauseMenu.SetActive(false); // <-- Currently throwing exception in Final_Environment.
        pData = GetComponent<PlayerData>();
        if (pData != null)
        {
            pData.LoadPlayer();
        }
    }

    private void Awake() 
    {
        //thePauseMenu = GameObject.Find("PauseScreenBackground");
        end = GetComponent<EndUI>();
        //pData = GetComponent<PlayerData>();
    }

    public void Caught(Transform robotsTransform)
    {
        gameOver = true;
        playerBody.rotation = robotsTransform.rotation;
        CMvcam1.GetComponent<CameraController>().caught = true;
        end.GameOver(); 
    }
    private int audioR = 0;
    private bool landed = false;
    bool firstStepsTaken;

    private void OnTriggerEnter(Collider collider)
    {
        // Future Chris: what are these first two for?
        if (collider.CompareTag("gdexWin"))
        {
            fadeOut.alphaFill();
        }

        if (collider.CompareTag("audioTick")&& audioR < 1)
        {
            audioR += 1;
        }

        if (collider.CompareTag("Stair"))
		{
            isOnStairs = true;
            playerSpeed = 60f;
		}

    }

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Stair"))
		{
            isOnStairs = false;
            playerSpeed = 40f;
		}
	}

	bool IsCapped()
    {
        RaycastHit hit;

        //Raycast upwards to check if ceiling is in the way
        if (Physics.Raycast(transform.position, Vector3.up, out hit,
            capRaycastDistance))
        {
            return true;
        }
        return false;
    }

    public void ButtonMouseLock()
    {
        if (!PauseMenu.GameIsPaused)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            ButtonMouseUnlock();
        }

    }

    public void ButtonMouseUnlock()
    {
        if (!PauseMenu.GameIsPaused)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

      
    }

    // FUTURE PROGRAMMERS --> Move these coroutines to NoiseWaypoint???-------------------------------------
    // These are the only pieces of code that can not be moved without losing functionality - Ali
    
    public IEnumerator StartRobotAttraction(float waitForGround)
    {
        yield return new WaitForSeconds(waitForGround);
        noiseWay.OnNoisemakerStart(Noplay.nMakeCurrent.transform.position.x, Noplay.nMakeCurrent.transform.position.z);
    }

    //END ABOVE COMMENT TO FUTURE PROGRAMMERS----------------------------------------------------------

    //CAN WE MOVE THIS TO A VIGNETTE SCRIPT/CAMERA/FOV?
    public void enableStealthMode(bool isEnabled, float deltaTime)
    {
        // Log info
        //if (vignetteEffect == null)
           // Debug.Log("vignetteEffect GameObject is null");

        // Set log string
       // Debug.Log(isEnabled ? "Stealth mode..." : "You are being chased...");

        // Set vignette effect
        Vignette vignetteComp = null;
        Vignette tmp;

        // Try to get the override component in Volume
        Volume volume = vignetteEffect.GetComponent<Volume>();
        if (volume.profile.TryGet<Vignette>(out tmp))
            vignetteComp = tmp;

        GameObject cameraObject = gameObject.transform.GetChild(0).gameObject;
        CinemachineVirtualCamera vcam = CMvcam1.GetComponent<CinemachineVirtualCamera>();
        if (isEnabled)
        {
            // Start Vignette & FOV easing
            StartCoroutine(startVignetteEasing(vignetteComp, vignetteComp.intensity.value, 0.4f));
            StartCoroutine(startFOVEasing(vcam, vcam.m_Lens.FieldOfView, ogFOV));

            // Disable camera shake
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
        }
        else
        {
           // Debug.Log("FOV = " + vcam.m_Lens.FieldOfView);
            StartCoroutine(startVignetteEasing(vignetteComp, vignetteComp.intensity.value, 0f));
            StartCoroutine(startFOVEasing(vcam, vcam.m_Lens.FieldOfView, toFOV));

            // Enable camera
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1.5f;
            vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 2;
        }
    }

    // Easing animation function for the change of FOV
    //CAN WE MOVE THIS TO A VIGNETTE SCRIPT/CAMERA/FOV?
    public IEnumerator startFOVEasing(CinemachineVirtualCamera vcam, float startVal, float endVal)
    {
        // Total time elapsed
        float timeElapsed = 0;

        // While the total time elapsed is smaller than designated duration
        // Start the easing animation
        while (timeElapsed < easingDuration)
        {
            // Easing function
            float t = timeElapsed / easingDuration;
            t = t * t * (3f - 2f * t);

            vcam.m_Lens.FieldOfView = Mathf.Lerp(startVal, endVal, t);
            timeElapsed += Time.deltaTime;

            yield return null;
        }

        // Apply value to camera FOV
        vcam.m_Lens.FieldOfView = endVal;
    }

    // Easing animation function for the vignette effect
    //CAN WE MOVE THIS TO A VIGNETTE SCRIPT/CAMERA/FOV?
    public IEnumerator startVignetteEasing(Vignette vignette, float startVal, float endVal)
    {
        float timeElapsed = 0;

        while (timeElapsed < easingDuration)
        {
            float t = timeElapsed / easingDuration;
            t = t * t * (3f - 2f * t);

            // Set the intensity of the vignette effect
            if (vignette != null)
                vignette.intensity.value = Mathf.Lerp(startVal, endVal, t);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        vignette.intensity.value = endVal;
    }

    void Update()
    {
        GetPlayerInput();

        // Check whether the player is grounded or not
  //      if (isGrounded())
		//{
  //          //Debug.Log("Grounded!");
		//}
  //      else
		//{
  //          //Debug.Log("Not on the ground");
		//}

        if ((camController.getsaboItem() != null && !inMinigame && !camController.getsaboItem().GetComponent<SabotageWaypoint>().isSabotaged) || stopHidingMouseIcon == true)
        {
            mouseIcon.SetActive(true);
        }
        else
        {
            mouseIcon.SetActive(false);
        }

        // If the player is being chased and call once is false
        if (isChasing && callOnceForVignette)
        {
            callOnceForVignette = false;
            enableStealthMode(false, Time.deltaTime);
        }

        // If the player is not being chased and call once is false
        if (!isChasing && !callOnceForVignette)
        {
            callOnceForVignette = true;
            enableStealthMode(true, Time.deltaTime);
        }

        if(!inMinigame && !gameOver)
        {

            // Player movement
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            if (!pausePlayer && (x != 0 || z != 0))
            {
                if (Input.GetButton("Crouch"))
                {
                    movementState = 1;
                }
                else
                {
                    movementState = 2;
                }
                
            }
            else
            {
                movementState = 0;
            }

            footstepEmitter.SetParameter("MovementState", movementState);


            // These two if statements cover the Up + Down Crouch audio for the player.
            if (Input.GetButtonDown("Crouch"))
            {
                AudioHandler.PlaySound("event:/s_Player_Crouch_DOWN", this.transform.position);
            }

            if (Input.GetButtonUp("Crouch"))
            {
                AudioHandler.PlaySound("event:/s_Player_Crouch_UP", this.transform.position);
            }

            if (isGrounded())
            {
                if (!landed)
                {
                    if (firstStepsTaken)
                    {
                        AudioHandler.PlaySound("event:/s_Player_Land", this.transform.position); // Land audio clip
                    }
                    else
                    {
                        firstStepsTaken = true;
                        //  LOOK, I HATE THIS.
                        //      BUT IT'S EASIER
                        //      THAN FIXING
                        //      IT IN FMOD.
                        //      PROBABLY.
                    }
                    
                    landed = true;
                }

                if (Input.GetButtonDown("Jump") && !pausePlayer)
                {
                   // Debug.Log("jump!");

                    playerRigidBody.AddForce(new Vector3(0, 2.0f, 0) * playerJumpForce, ForceMode.Impulse);

                    AudioHandler.PlaySound("event:/s_Player_Jump", this.transform.position); // Jump audio clip
                    landed = false;

                }
            }

            if (Input.GetButton("Crouch"))
            {
                playerBody.localScale = CrouchScale;
                raycastDistance = 0.33f;
                playerSpeed = 10f;
            }
            else if (raycastDistance < 0.55f && !IsCapped())
            {
                playerBody.localScale = FullScale;
                velocity.y += 20f;
                // Need to change to rigidbody movement
                //controller.Move(velocity * Time.deltaTime);
                velocity.y -= 20f;
                raycastDistance = 0.55f;
                playerSpeed = 15f;
            }
			else
			{
                if (!isOnStairs || !isGrounded())
                {
                    playerSpeed = 40f;
                }
                else
                {
                    //velocity.y = -80f;
                    playerRigidBody.AddForce(new Vector3(0, -2f, 0), ForceMode.Impulse);
                }
			}
            Vector3 move = transform.right * x + transform.forward * z;
            currentVelocity = move + velocity;
            //controller.Move(move * playerSpeed * Time.deltaTime);

            // Player movement speed
            //controller.Move(velocity * Time.deltaTime);
            if (!isGrounded())
            {
                velocity.y += gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
            }

            if (Input.GetMouseButtonDown(0) /*&& nearby != null*/)
            {
                //print("mouseclick");
                if(camController.getsaboItem() != null && !inMinigame && !camController.getsaboItem().GetComponent<SabotageWaypoint>().isSabotaged)
                {
                    print("entered if");
                    inMinigame =true;
                    camController.flipLock();
                    camController.saboItem.GetComponent<Sabotage>().AttemptSabo();
                    AudioHandler.PlaySound("event:/s_Player_Hack", this.transform.position); // Hacking SFX.
                }
                else if (inMinigame)
                {
                    inMinigame = false;
                    camController.flipLock();
                }
                checking = true;
                addTex = false;
                active = false;
                Invoke("NotSus", 3.0f);
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0)/*&& nearby != null*/)
                {
                    inMinigame = false;
                    camController.flipLock();
                }
            if (camController.caught && camController.saboItem != null)
            {
                camController.saboItem.GetComponent<SabotageMinigame>().Failure();
            }
        }

        if (Input.GetKeyDown("z"))
        {
            if (thePauseMenu.activeInHierarchy == false)
            {
                Time.timeScale = 0;
                thePauseMenu.SetActive(true);
                ButtonMouseUnlock();
            }
            else
            {
                Time.timeScale = 1;
                thePauseMenu.SetActive(false);
                ButtonMouseLock();
            }
        }

        if (Input.GetKeyDown("u"))
        {
            ButtonMouseUnlock();
        }

        if (Input.GetKey("r"))
        {
            AudioHandler.FadeOutLevelMusic();
            AudioHandler.StopEverything();
            Scene scene = SceneManager.GetActiveScene(); 
            gameOver = false;
            //Time.timeScale = 1;
            SceneManager.LoadScene(scene.name);
        }

    }


    private void FixedUpdate()
    {
        // Apply player movement
        PlayerMovement();
    }

    // This function is for player input only
    void GetPlayerInput()
    {
        // Get movement input
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        verticalMovement = Input.GetAxisRaw("Vertical");

        // Hold left shift to run faster
        if (Input.GetKeyDown(KeyCode.LeftShift) && !gameOver)
        {
            playerSpeed += 25f;
            isChasing = true;
           // Debug.Log("Fast run!!!!");
        }

        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            playerSpeed -= 25f;
            isChasing = false;
          //  Debug.Log("Normal run...");
        }

        // Calculate moving directions
        moveDirection = transform.forward * verticalMovement + transform.right * horizontalMovement + transform.up * velocity.y;
    }

    void PlayerMovement()
    {
        // Move the player (mass-dependent)
        if(!gameOver && !pausePlayer)
        {
            playerRigidBody.AddForce(moveDirection.normalized * playerSpeed, ForceMode.Acceleration);
        }
    }

    // A function that checks whether the player is currently grounded or not
    bool isGrounded()
    {
        return (
            // Down straight
            Physics.Raycast(transform.position, Vector3.down, 1.0f) ||

            // Down-left 45 degrees
            Physics.Raycast(transform.position, (Vector3.down + Vector3.left).normalized, 1.0f) ||

            // Down-right 45 degrees
            Physics.Raycast(transform.position, (Vector3.down + Vector3.right).normalized, 1.0f)
        );


    }

    public void ChangePlayerRotation()
    {
        // LOOK, I HATE IT TOO.
        // BUT SOMETIMES YOU JUST GOTTA
        // RAZZLE DAZZLE 'EM. -C.
        Vector3 rot = new Vector3(0, -90f, 0);
        this.gameObject.transform.rotation = Quaternion.Euler(rot);
    }
}
