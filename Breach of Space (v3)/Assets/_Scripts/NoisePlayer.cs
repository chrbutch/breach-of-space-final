using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NoisePlayer : MonoBehaviour
{
    public NoisemakerWaypoint noiseWay;     // Retrieves the OnNoisemakerStart() and OnNoisemakerEnd() functions from NoisemakerWaypoint
    public PlayerMove playMove;             // Instance of PlayerMove script
    private Rigidbody playerRigidBody;
    public Rigidbody nMakeRigid;            // Gets the rigid body of an instanced Noisemaker
    public GameObject nMakeStation;         // The station where you receive the noisemaker
    public GameObject nMakePrefab;          // The prefab for making noisemakers
    public GameObject nMakeInstance = null; // References the newly instanced noisemaker
    public GameObject nMakeCurrent;         // Stores the oldest noisemaker in a variable that can be destroyed after a coroutine
    public GameObject explosionEffect;      // The explosion effect that happens when the noisemaker is destroyed
    public bool isParented = true;          // Is the noisemaker a child of the player
    public bool nMakeInHand = false;        // Triggered when noisemaker is in the inventory
    public bool activeNMakeDeleted = true;  // Used to see when the thrown noisemaker has been deleted
    public int throwPower = 1;              // The strength of the throw
    public float arcHeight = 6f;            // The peak height of the throw
    public float gravity = -9.8f;           // The downward force
    public float nMakeWaypointDelay = 2f;   // The delay that allows the noisemaker to hit the ground before setting the waypoint
    public float nMakeExistTime = 15f;      // The time it takes before the noisemaker disappears

    // Can we make these a Transform[]?
    [Header("Noisemaker Trajectory")]
    public Transform targetT; // The empty gameobject the acts as a target for the throw
    public GameObject targetS; // The shortest target
    public GameObject targetM; // The mid-range target
    public GameObject targetL; // The farthest target

    public GameObject clickIcon; // Mouse Icon wasn't working for some reason
    public Transform currPlayPos; // Constantly gives an updated player position
    private void Start()
    {
        playerRigidBody = GetComponent<Rigidbody>();
        throwPower = 1;
        turnOffAllTargets();
    }

    private void Awake()
    {
        currPlayPos = this.transform;
        playMove = FindObjectOfType<PlayerMove>();
        nMakeStation = GameObject.FindGameObjectWithTag("NoiseStation");
        targetS = GameObject.Find("TargetTS");
        targetM = GameObject.Find("TargetTM");
        targetL = GameObject.Find("TargetTL");
    }

    void Update()
    {
        
        if (nMakeInstance != null && nMakeInstance.transform.parent == null)
        {
            isParented = false;
        }
        else
        {
            isParented = true;
        }// WE SHOULD FIX THIS! - C.

        if (nMakeInstance == null)
        {
            activeNMakeDeleted = true;
            nMakeRigid = null;
        }
        else
        {
            activeNMakeDeleted = false;
            nMakeRigid = nMakeInstance.GetComponent<Rigidbody>();
        }

        if (Input.GetMouseButtonDown(1) && nMakeInHand == true)
        {
            activeNMakeDeleted = false;
            // Instantiate the noisemaker
            nMakeInstance = Instantiate(nMakePrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z) + (transform.forward * 1), Quaternion.identity);
            nMakeInstance.AddComponent<NoisemakerWaypoint>();
            noiseWay = nMakeInstance.GetComponent<NoisemakerWaypoint>();
            playMove.noiseWay = noiseWay;
            nMakeInstance.transform.parent = gameObject.transform;
            // Make the NMake invisable and the shortest throw range visable
            nMakeInstance.SetActive(false);
            targetS.SetActive(true);
            targetT = targetS.transform;
            throwPower = 1;
        }

        if (Input.GetKey(KeyCode.Mouse1) && nMakeInHand == true)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                if (throwPower == 2)
                {
                    throwPower -= 1;
                    targetS.SetActive(true);
                    targetM.SetActive(false);
                    targetL.SetActive(false);
                    targetT = targetS.transform;
                }
                else if (throwPower == 3)
                {
                    throwPower -= 1;
                    targetM.SetActive(true);
                    targetL.SetActive(false);
                    targetS.SetActive(false);
                    targetT = targetM.transform;
                }
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                if (throwPower == 2)
                {
                    throwPower += 1;
                    targetL.SetActive(true);
                    targetM.SetActive(false);
                    targetS.SetActive(false);
                    targetT = targetL.transform;
                }
                else if (throwPower == 1)
                {
                    throwPower += 1;
                    targetM.SetActive(true);
                    targetL.SetActive(false);
                    targetS.SetActive(false);
                    targetT = targetM.transform;
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse1) && nMakeInHand == true)
        {
            nMakeInstance.SetActive(true);
            // Gets the rigid body of the instance
            nMakeRigid = nMakeInstance.GetComponent<Rigidbody>();
            nMakeRigid.useGravity = false;
            // Throw the noisemaker
            FindThrowPower();
            ThrowItem();
            nMakeInHand = false;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("NoiseStation") && activeNMakeDeleted == true)
        {
            playMove.stopHidingMouseIcon = true;
            nMakeStation = collider.gameObject;
        }

    }
    public void OnTriggerStay(Collider play)
    {
        if (play.CompareTag("NoiseStation"))
        {
            if (Input.GetMouseButtonDown(0) && nMakeInHand == false)
            {
                nMakeStation.SetActive(false);
                nMakeInHand = true;
                AudioHandler.PlaySound("event:/s_Player_Crafting", this.transform.position);
                playMove.stopHidingMouseIcon = false;
            }
            else if (Input.GetMouseButtonDown(0) && nMakeInHand == true)
            {
                //Debug.LogWarning("NMake already in hand!");
            }
            else if (Input.GetMouseButtonDown(0))
            {
                //Debug.LogWarning("Something went horribly wrong! Check OnTriggerStay in PlayerMove.");
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("NoiseStation"))
        {
            playMove.stopHidingMouseIcon = false;
            nMakeStation.SetActive(true);
        }

    }

    public void turnOffAllTargets()
    {
        targetS.SetActive(false);
        targetM.SetActive(false);
        targetL.SetActive(false);
    }

    public void FindThrowPower()
    {
        if (throwPower == 1)
        {
            arcHeight = 1;
            targetS.SetActive(true);
            targetT.position = targetS.transform.position;
        }
        else if (throwPower == 2)
        {
            arcHeight = 1;
            targetM.SetActive(true);
            targetT.position = targetM.transform.position;
        }
        else if (throwPower == 3)
        {
            arcHeight = 1;
            targetL.SetActive(true);
            targetT.position = targetL.transform.position;
        }
        else
        {
           // Debug.LogWarning("Something went wrong if the FindThrowPower function!!");
        }
    }
    public void ThrowItem()
    {
        if (playMove.isChasing == false)
        {
            Physics.gravity = Vector3.up * playMove.gravity;
            nMakeRigid.useGravity = true;
            nMakeRigid.velocity = FindLaunchVelocity();
            if (nMakeRigid.useGravity == true)
            {
                nMakeRigid.transform.parent = null;
            }
            turnOffAllTargets();
            playMove.StartCoroutine(playMove.StartRobotAttraction(nMakeWaypointDelay));
            nMakeCurrent = nMakeInstance;
            nMakeInHand = false;
        }
    }
    Vector3 FindLaunchVelocity()
    {
        float displaceY = targetT.transform.position.y - transform.position.y;
        Vector3 displaceXZ = new Vector3(targetT.transform.position.x - transform.position.x, 0, targetT.transform.position.z - transform.position.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * playMove.gravity * arcHeight);
        Vector3 velocityXZ = displaceXZ;

        return velocityXZ + velocityY;
    }

    public void DestroyNoisemaker()
    {
        Instantiate(explosionEffect, nMakeCurrent.transform.position, nMakeCurrent.transform.rotation);
        activeNMakeDeleted = true;
        nMakeStation.SetActive(true);
        // AND WHATEVER ELSE SHOULD GO HERE?
    }
    
}
