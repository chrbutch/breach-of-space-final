using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.SceneManagement;

public class EndCutsceneHelper : MonoBehaviour
{
    SoundPlayer soundPlayer;

    void Awake()
    {
        soundPlayer = GetComponent<SoundPlayer>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            AudioHandler.FadeOutLevelMusic();
            AudioHandler.StopEverything();
            SceneManager.LoadScene("FinalScene");
        }
    }
    public void PlayUsOut()
    {

        soundPlayer.PlaySound("event:/m_BTO");
        SceneManager.LoadScene("Credits");
        //I had originally used this method for everything but
        //  time is of the essence, soooooooooooooooooooooooo.
    }
    public void StartWarp() => WarpSpeed.warpActive = true;
    public void Explode()
    {
        // PLAY EXPLOSION HERE
    }
}
