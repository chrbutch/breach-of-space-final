using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderLookAt : MonoBehaviour
{
    public GameObject currentlyShadered;

    // Start is called before the first frame update
    void Start()
    {
        currentlyShadered = null;
    }

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 5f))
        {
            //Debug.Log("$$$" + hit.collider.gameObject.name);
            if (hit.collider.gameObject.tag == "SABO")
            {
                //Debug.Log("?!? - SABO'd!");
                if(currentlyShadered != null)
                {
                    //Debug.Log("?!? - null'd!");
                    if (currentlyShadered != hit.collider.gameObject)
                    {

                        //currentlyShadered.GetComponentInParent<EPOOutline.Outlinable>().enabled = false;
                        currentlyShadered = null;
                        currentlyShadered = hit.collider.gameObject;
                        //currentlyShadered.GetComponentInParent<EPOOutline.Outlinable>().enabled = true;
                    }
                }
                else
                {
                    currentlyShadered = hit.collider.gameObject;
                    //currentlyShadered.GetComponentInParent<EPOOutline.Outlinable>().enabled = true;
                }
            }
            else
            {
                if(currentlyShadered != null)
                {
                    //currentlyShadered.GetComponentInParent<EPOOutline.Outlinable>().enabled = false;
                    currentlyShadered = null;
                }
            }
        }
        else
        {
            
        }
    }
}
