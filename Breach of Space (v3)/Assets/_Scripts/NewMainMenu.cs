using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;

public class NewMainMenu : MonoBehaviour
{
    public GameManager manager;

    public static bool isSettings = false;

    public GameObject settingsMenu;
    public GameObject mainMenu;
    public FMODUnity.StudioEventEmitter emitter;

    public void Awake()
    {
        manager = GameManager.instance;
    }
    // Start is called before the first frame update
    public void PlayGame()
    {
        //manager.LoadGame(); //calls void LoadGame() in game manager script 
        SceneManager.LoadScene("Final_Environment");
        emitter.Stop();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void Settings()
    {
        isSettings = true;
        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }
    public void ExitSettings()
    {
        mainMenu.SetActive(true);
        settingsMenu.SetActive(false);
        isSettings = false;

    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isSettings)
            {
                ExitSettings();
            }
        }
    }
}