using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2021.09.19 @ 08:25 PM - CB.
public class PatrolWaypoint : Waypoint
{
    public float timeHere; // Amount of time Robot stands here (defaults to 0)

    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        if(timeHere > 0)
        {
            //Debug.Log(timeHere);
            thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
            yield return new WaitForSeconds(timeHere);
            if(thisRobot.patrolPositions.Length > 1)
            {
                thisRobot.SetRobotStatus(Robot.RobotStates.Walking);
            }
            //Debug.Log("OK!");
        }
        thisRobot.SetNextRobotWaypoint();
        //Debug.Log("<color=cyan>WAYPOINT " + this.name + " COMPLETED</color>");
        yield return null;
    }
}
