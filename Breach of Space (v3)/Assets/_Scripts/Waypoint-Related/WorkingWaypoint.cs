using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// 2021.11.04 @ 04:22 PM - CB.
public class WorkingWaypoint : Waypoint
{
    public GameObject lookDirection;                // Where the Robot is looking toward.
    public float workingTime;                       // How long the Robot spends here.
    bool lookingAtTarger;                           // Whether the Robot is looking at the target.
    public FMODUnity.StudioEventEmitter emitter;    // Emitter for sound.

    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        lookingAtTarger = false;
        thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
        StartCoroutine(LookAt(thisRobot));
        while (!lookingAtTarger)
        {
            yield return null;
        }
        //thisRobot.transform.LookAt(lookDirection.transform.position);
        //yield return new WaitForSeconds(1);
        thisRobot.SetRobotStatus(Robot.RobotStates.Working);
        emitter.SetParameter("Working", 1);
        emitter.Play();
        yield return new WaitForSeconds(workingTime);
        emitter.SetParameter("Working", 0);
        thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
        yield return new WaitForSeconds(1);
        if(thisRobot.patrolPositions.Length > 1)
        {
            thisRobot.SetRobotStatus(Robot.RobotStates.Walking);
        }
        thisRobot.SetNextRobotWaypoint();
        yield return null;
    }

    IEnumerator LookAt(Robot theRobot)
    {
        Vector3 fixedLocation = new Vector3(
            lookDirection.transform.position.x, theRobot.transform.position.y, 
            lookDirection.transform.position.z);
        Quaternion lookRotation = Quaternion.LookRotation(fixedLocation - theRobot.transform.position);
        float timer = 0;
        while(timer < 1)
        {
            theRobot.transform.rotation = Quaternion.Slerp(theRobot.transform.rotation, lookRotation, timer);
            timer += Time.deltaTime;
            yield return null;
        }
        lookingAtTarger = true;
        yield return null;
    }
}
