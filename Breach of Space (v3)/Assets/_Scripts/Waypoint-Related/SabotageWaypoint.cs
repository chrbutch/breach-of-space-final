using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2021.11.04 @ 04:25 PM - CB.
public class SabotageWaypoint : WorkingWaypoint
{
    public bool isSabotaged;
    public GameObject partParent;
    public Vector3 targetPosition = new Vector3(0,0,0);

    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        //print("reached waypoint");
        thisRobot.transform.LookAt(lookDirection.transform.position);
        thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
        yield return new WaitForSeconds(1);
        thisRobot.SetRobotStatus(Robot.RobotStates.Working);
        emitter.SetParameter("Fixed", 0);
        emitter.Play();
        yield return new WaitForSeconds(workingTime);
        foreach(Transform child in partParent.transform)
        {
            child.gameObject.GetComponent<ParticleSystem>().Stop();
        }
        emitter.SetParameter("Fixed", 1);
        thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
        yield return new WaitForSeconds(1);
        // more stuff, cause sabo.
        //  TURN OFF PARTICLES
        isSabotaged = false;
        thisRobot.SetRobotStatus(Robot.RobotStates.Walking);
        thisRobot.SetNextRobotWaypoint();
        //  OTHER VARIABLES NECESSARY FOR PLAYER PROGRESSION.
        yield return null;
    }

    public void Broken()
    {
        isSabotaged = true;
    }
}
