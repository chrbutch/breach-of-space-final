using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OMDWaypoint : Waypoint
{
    // 2021.11.30 @ 03:48 PM - JS.

    public Vector3 waypointLocation;
    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        thisRobot.busy = true;
        Debug.Log("<color=lime>HEY, " + thisRobot + " is doing it!</color>");
        int lookAmount = Random.Range(1, 5);
        for(int i = 0; i < lookAmount; i++)
        {
            thisRobot.SetRobotStatus(Robot.RobotStates.Searching);
            float randomX = Random.Range(this.transform.position.x - 1, this.transform.position.x + 1);
            float randomZ = Random.Range(this.transform.position.z - 1, this.transform.position.z + 1);
            Vector3 randomVector = new Vector3(randomX, this.transform.position.y, randomZ);
            thisRobot.SetRobotDestination(randomVector);
            while (!thisRobot.AtDestination())
            {
                yield return null;
            }
            thisRobot.SetRobotStatus(Robot.RobotStates.Idle);
            //Debug.Log("Sus. Waypoint Working! " + i + ": " + randomVector);
            yield return new WaitForSeconds(Random.Range(0.5f, 2f));
        }
        // Shrug
        thisRobot.SetNextRobotWaypoint();
        thisRobot.ReturnToWork();
        Destroy(this.gameObject);
        yield return null;
    }
}
