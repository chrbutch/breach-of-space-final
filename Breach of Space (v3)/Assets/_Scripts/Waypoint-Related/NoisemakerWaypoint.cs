using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

// 2022.02.02 @ 03:54 PM - CB.
public class NoisemakerWaypoint : Waypoint
{
    public NoisePlayer getPlayer;

    [Header("Animation Booleans")]
    public bool hasBeenPickedUp = false;
    public bool hasBeenThrown = false;

    public Rigidbody rb;
    Robot robot;
    List<Robot> deafenedRobots = new List<Robot>();
    Vector3 waypointLocation;

    [Header("FMOD")]
    public FMODUnity.StudioEventEmitter emitter; // delete if handled in Noisemaker code ???????????
    float quickTime = 2f;
    float timer;            // handled elsewhere?
    float timer_MAX = 15f;  // handled elsewhere?

    void Start()
    {
        getPlayer = FindObjectOfType<NoisePlayer>();
        rb = GetComponent<Rigidbody>();
        emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        timer = timer_MAX;
        
    }

    void FixedUpdate()
    {
        if (hasBeenThrown)
        {
            if (robot != null && hasBeenPickedUp)
            {
                this.gameObject.transform.position = robot.itemHand.transform.position;
            }
            else
            {
                // decrease timer here??
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    OnNoisemakerEnd();
                }
            }
        }
    }

    public override IEnumerator WaypointBehavior(Robot thisRobot)
    {
        // --- NEW STUFF ---
        timer = 2f;// Time before Robot throws it + y.r.n.WFS.
        //Debug.LogWarning("Noise Way Behave is triggered");
#if UNITY_EDITOR
        UnityEditor.EditorGUIUtility.PingObject(this.gameObject); // OH NO, IT IS ME.
#endif
        // Set Robot to idle
        //robot = RobotBlackboard.GetNearestRobot(this.transform.position); <- unnecessary, already given as
        //                                                                      param. fr. Blackboard.
        robot = thisRobot;
        robot.SetRobotStatus(Robot.RobotStates.Idle);
        yield return new WaitForSeconds(0.6f);
        // GRAB ANIMATION (check Rokoko)
        //robot.anim.SetTrigger("pickUp");
        // robot.GetComponent<Robot>().anim.ResetTrigger("pickUp");
        // For Chris --> Please see if this works <--
        robot.NoisemakerPickUp(this.gameObject);
        // noisemaker attaches to the Robot hand.
        //robot.AttachToHand(this.gameObject);
        //robot.anim.ResetTrigger("pickUp"); //?

        // HOLD FOR HOWEVER LONG THE NOISEMAKER HAS LEFT (including a minimum to finish animation)
        // explode anim. or whatever
        // OnNoisemakerEnd(); <- if we aren't using Invoke in OnNoisemakerStart()........
        //Debug.LogWarning("Noise Way Behave is done");
        yield return null;
    }

    /*public IEnumerator StartRobotAttraction(float waitForGround)
    {
        // Debug.LogWarning(" --- Robot Attraction Delay Started --- ");
        yield return new WaitForSeconds(waitForGround);
        OnNoisemakerStart(getPlayer.nMakeCurrent.transform.position.x, getPlayer.nMakeCurrent.transform.position.z);
        // Debug.LogWarning(" --- Robot Attraction Started - Delay Over --- ");
        StartCoroutine(DestoryCurrentNMake(getPlayer.nMakeExistTime));
    }

    public IEnumerator DestoryCurrentNMake(float destroyTime)
    {
        // Debug.LogWarning(" --- Destroy NMake Coroutine Started --- ");
        yield return new WaitForSeconds(destroyTime);
        OnNoisemakerEnd();
        Instantiate(getPlayer.explosionEffect, getPlayer.nMakeCurrent.transform.position, getPlayer.nMakeCurrent.transform.rotation);
        Destroy(getPlayer.nMakeCurrent);
        getPlayer.activeNMakeDeleted = true;
        // Debug.LogWarning(" --- Destroy NMake Coroutine Ended --- ");
    }*/

    public void OnNoisemakerStart(float xValue, float zValue)
    {
        //.EditorGUIUtility.PingObject(this.gameObject); // Future Chris, take this out. Thanks!
        robot = RobotBlackboard.GetNearestRobot(this.transform.position);
        robot.RobotNoisemaker = this;
        // robot.currentWaypoint = (Waypoint)this;
        RobotBlackboard.robotActivities[robot] = (Waypoint)this;
        waypointLocation = new Vector3(xValue, 0.5f, zValue);       // The y val. will not always be 0.5f? -c.

        NavMesh.SamplePosition(waypointLocation, out NavMeshHit hit, 1, 1);
        waypointLocation = new Vector3(waypointLocation.x, hit.position.y, waypointLocation.z - 1.5f);
        //GameObject noisemakerWaypoint = new GameObject("Noisemaker Waypoint", typeof(NoisemakerWaypoint));
        GameObject noisemakerWaypoint = this.gameObject; // CLEAN THIS UP.
        //              OK, SO the above line really bugs me,
        //              because we're creating a NEW version of THIS
        //              for the Robot. Which is kind of silly.
        //              So maybe this should be moved/changed/whatever. - c. 1/17
        // FUTURE CHRIS:__________________
        // ADD FMOD STUDIO EVENT EMITTER__
        // MAKE EVENT THE RIGHT EVENT_____
        // START EVENT.___________________
        // THANK YOUUUUUUu________________
        //Debug.LogWarning("Error throw");
        noisemakerWaypoint.transform.position = waypointLocation;
        RobotBlackboard.robotActivities[robot] = noisemakerWaypoint.GetComponent<NoisemakerWaypoint>();
        robot.currentWaypoint = RobotBlackboard.robotActivities[robot];
        robot.SetRobotStatus(Robot.RobotStates.Curious);
        robot.SetRobotDestination(RobotBlackboard.robotActivities[robot].transform.position);
        robot.PlayVoiceline("event:/s_Robot_AHA");
        //RobotBlackboard.ReportSuspiciousActivity(robot, playerM.targetT.position);
        //emitter.SetParameter("Destroyed", 0f);
        emitter.Play();
        hasBeenThrown = true;  
        // DEAFEN ALL ROBOTS IN THE AREA EXCEPT THE ONE.
        //      - OnTriggerEnter/Exit???
        //      - Distance-based???
        
        // Invoke("OnNoisemakerEnd", 10f); // The 2nd arg. should be the time the noisemaker is functional. !!!
        // ^^^ I commented this out for now since I call OnNoisemakerEnd in the PlayerMove script ^^^ 
    }

    public void OnNoisemakerEnd()
    {
        hasBeenThrown = false; // I don't want to make another bool.
        getPlayer.DestroyNoisemaker();
        if(robot != null)
        {
            RobotBlackboard.robotActivities[robot] = null;
            robot.SetRobotStatus(Robot.RobotStates.Idle);
            robot.SetNextRobotWaypoint();
            robot.SetRobotStatus(Robot.RobotStates.Walking);
            robot.RobotNoisemaker.gameObject.SetActive(false); //<----------------------
            robot.RobotNoisemaker = null;
        }
        foreach(Robot r in deafenedRobots)
        {
            r.SetRobotStatus(Robot.RobotStates.Idle); // undeafens robots
        }
        
        deafenedRobots.Clear();
        emitter.SetParameter("Destroyed", 1f);
        
        this.robot = null;
        //hasBeenPickedUp = false;   
        Destroy(this.gameObject);
        //gameObject.SetActive(false);   // WE SHOULD STORE THIS, BECAUSE DESTROYING
                                            // PREFABS CAUSES ISSUES. THANKS -c.
    }

    IEnumerator QuickDestroyNMake(float quickTimer)
    {
        yield return new WaitForSeconds(quickTimer);
        OnNoisemakerEnd();
        Instantiate(getPlayer.explosionEffect, this.transform.position, this.transform.rotation);
        //Destroy(this.gameObject);
        getPlayer.activeNMakeDeleted = true;
        yield return null;
    }

    public void RobotNoisemakerAnimationUpdate() => hasBeenPickedUp = !hasBeenPickedUp;

    public void RobotDestroysNoisemaker()
    {
        RobotNoisemakerAnimationUpdate();
        rb.AddForce(robot.transform.forward, ForceMode.Force);
        rb.velocity = robot.transform.forward * 15f;
        // BOOM?!
    }
}
