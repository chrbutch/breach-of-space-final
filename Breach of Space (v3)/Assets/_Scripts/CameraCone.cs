using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class CameraCone : MonoBehaviour
{

    public GameObject gameoverscene;
    public FMODUnity.StudioEventEmitter emitter;

    Animator cameraAnim;

    Renderer CamRenderer;
    // Start is called before the first frame update
    private void Start()
    {
        cameraAnim = GetComponent<Animator>();
        CamRenderer = GetComponent<Renderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            cameraAnim.enabled = false;
            Color redcolor = new Color(0.6f, 0.1f, 0.1f, 0.3f);
            CamRenderer.material.SetColor("_TintColor", redcolor);
            StartCoroutine(StopCamera());
            RobotBlackboard.ReportSuspiciousActivity(RobotBlackboard.GetNearestRobot(other.transform.position),other.transform.position);
        }
    }

    // Update is called once per frame
    IEnumerator StopCamera()
    {
        yield return new WaitForSeconds(0.5f);
        gameoverscene.SetActive(true);
    }
}
