using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseFunctions : MonoBehaviour
{
    public GameObject thePauseMenu;

    private void Awake()
    {
        //thePauseMenu = GameObject.Find("PauseScreenBackground");
    }

    public void resumeGame()
    {
        thePauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void optionsMenu()
    {
        // This is where an options menu would be opened
    }

    public void returnMainMenu()
    {
        SceneManager.LoadScene("StartScreen");
    }
}
