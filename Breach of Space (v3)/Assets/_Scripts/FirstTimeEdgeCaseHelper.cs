using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTimeEdgeCaseHelper : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            AudioHandler.gameMusic.setParameterByName("Intro", 3);
            this.gameObject.SetActive(false);
        }
    }
}
