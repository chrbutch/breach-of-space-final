using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sabotage : MonoBehaviour
{
    public bool boomer = false;
    public bool flippable = false;
    public GameObject partParent;
    public VentBoom vent;
    public FlipObject flip;
    public GameObject storyBoard;
    public bool broken = false;
    // Start is called before the first frame update

    public void AttemptSabo()
    {
        GetComponent<SabotageMinigame>().SpawnGame();
    }

    public void Break()
    {
        //print("break called");
        if (boomer)
        {
            vent.Explode();
        }
        else if (!flippable)
        {
            RobotBlackboard.TurnOnSabotage(this.GetComponent<SabotageWaypoint>());
        }
        if(partParent != null)
        {
            foreach(Transform child in partParent.transform)
            {
                child.gameObject.GetComponent<ParticleSystem>().Play();
            }
        }
        
        /*if (AudioHandler.chaos < 0.36f)
        {
            AudioHandler.chaos = 0.36f;
        }*/
        
        if (flippable)
        {
            flip.Flip();
        }
        GetComponent<SabotageWaypoint>().Broken();

        if(storyBoard != null)
        {
            storyBoard.SetActive(true);
        }
        
    }

    public bool IsBroken()
    {
        return broken;
    }
}
