using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class alphaSpammer : MonoBehaviour
{
    // Start is called before the first frame update
    private bool filling = false;
    public GameObject winRender;
    private bool winRendering = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (filling)
        {
        float alpha = gameObject.GetComponent<Image>().color.a;
         
            if(alpha < 1) 
            {
                alpha += Time.deltaTime;
                if (alpha > 0.95)
                {
                    //print("RENDERPOPUP");
                    winRendering = true;
                }
                else
                {
                    //print("RENDERPOPUP FAIL "+alpha);
                }
                Color newColor = new Color(0, 0, 0, alpha);
                gameObject.GetComponent<Image>().color = newColor;
            }
        }
        if (winRendering)
        {
            winRender.GetComponent<CanvasGroup>().alpha += Time.deltaTime;
        }
    }

    public void alphaFill()
    {
        filling = true;
        AudioHandler.gameMusic.setParameterByName("LevelCompleted", 2f);
    }
}
