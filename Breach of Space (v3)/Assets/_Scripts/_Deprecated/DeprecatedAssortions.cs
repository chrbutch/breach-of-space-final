using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeprecatedAssortions : MonoBehaviour
{
    /*
    -----------------------------------------------------------------------------
    if (Input.GetKeyDown("o") && scrapIsOpen == false && craftIsOpen == false)
    {
        // Open the scrap menu
        Debug.LogWarning("Scrap Menu Open");
        scrapMenu.SetActive(true);
        scrapIsOpen = true;
    }
    else if (Input.GetKeyDown("o") && scrapIsOpen == true)
    {
        // Close the scrap menu
        Debug.LogWarning("Scrap Menu Closed");
        scrapMenu.SetActive(false);
        scrapIsOpen = false;
    }

    // Below is the code for the opening and crafting scrap
    // P = Open Menu, O = Craft Noisemaker, L = Scrap Crafted Item

    if (Input.GetKeyDown("p"))
    {
        Debug.LogWarning("Craft Menu Open");
    }
    if (Input.GetKey("p"))
    {
        // Open the craft menu
        
        craftMenu.SetActive(true);
        
        if (Input.GetKeyUp("o"))
        {
            kraft.DoCraft();
            craftMenu.SetActive(false);
        }
        if (Input.GetKeyUp("l"))
        {
            kraft.UndoCraft();
            craftMenu.SetActive(false);
        }
    }
    if (Input.GetKeyUp("p"))
    {
        // Close the craft menu
        Debug.LogWarning("Craft Menu Closed");
        craftMenu.SetActive(false);
        dTxxt.ClearTxt();
    }
    else
    {
        //ButtonMouseLock();
    }
    -----------------------------------------------------------------------------
    if (nMakeInstance != null && nMakeInstance.transform.parent == null)
    {
        isParented = false;
    }
    else
    {
        isParented = true;
    }
    -----------------------------------------------------------------------------
    // currPlayPos.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);
    // This code is for preparing the noisemaker throw by putting a noisemaker behind the player
    if (kraft.itemInUse == true && Input.GetKeyDown("b"))
    {
        ThrowItem();
        Debug.LogWarning(currPlayPos.transform.position);
    }-----------------------------------------------------------------------------
    if (checking)
    {
        player can't do anything other than click buttons
    }
    Ltran.texture = objImg.tex;
    if (checking)
    {
        direction = new Vector3(0f, 0f, 0f).normalized;
    }
    if(direction.magnitude >= 0.1f)
    {
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

        Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

        controller.Move(moveDir.normalized * playerSpeed * Time.deltaTime);
    }
    -----------------------------------------------------------------------------
    [Header("Noisemaker")]                                                          // THESE SHOULD HAVE THEIR OWN SCRIPT -c.
    // These are for retrieving, instantiating, and throwing the noisemaker ---> //
    public NoisemakerWaypoint noiseWay; // Retrieves the OnNoisemakerStart() and OnNoisemakerEnd() functions from NoisemakerWaypoint
    public Rigidbody nMakeRigid; // Gets the rigid body of an instanced Noisemaker
    public GameObject nMakeStation; // The station where you receive the noisemaker
    public GameObject nMakePrefab; // The prefab for making noisemakers
    public GameObject nMakeInstance; // References the newly instanced noisemaker
    public GameObject nMakeCurrent; // Stores the oldest noisemaker in a variable that can be destroyed after a coroutine
    public GameObject explosionEffect; // The explosion effect that happens when the noisemaker is destroyed
    public bool isParented = true; // Is the noisemaker a child of the player
    public bool nMakeInHand = false; // Triggered when noisemaker is in the inventory
    public bool activeNMakeDeleted = true; // Used to see when the thrown noisemaker has been deleted
    public int throwPower = 1; // The strength of the throw
    public float arcHeight = 6f; // The peak height of the throw
    public float nMakeWaypointDelay = 2f; // The delay that allows the noisemaker to hit the ground before setting the waypoint
    public float nMakeExistTime = 5f; // The time it takes before the noisemaker disappears
    public GameObject levelThrow; // This references the throwPower UI
    public Transform targetT; // The empty gameobject the acts as a target for the throw
    public GameObject targetS; // The shortest target
    public GameObject targetM; // The mid-range target
    public GameObject targetL; // The farthest target
    public GameObject clickIcon; // Mouse Icon wasn't working for some reason
    Vector3 FindThrowDistance; // The equation used to add force to the throw
    <--- These are for retrieving, instantiating, and throwing the noisemaker
    -----------------------------------------------------------------------------
    */
}
