﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChaosBar : MonoBehaviour
{
    public GameObject sliderobject;
    public EndUI end;
    public Slider slider;
    private float currentValue;
    private float comboTimer = 0f;
    private int currentCombo = 0;
    public Text comboText;

    public float CurrentValue {
        get {
            return currentValue;
        }
        set {
            currentValue = value;
            slider.value = currentValue;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        comboText.text = "";
        currentValue = slider.value;
        slider = sliderobject.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = currentValue;
        if (comboTimer > 0f)
        {
            comboTimer -= Time.deltaTime;
            if(comboTimer <= 0f)
            {
                currentCombo = 0;
            }
            comboText.text = "";
        }

        //AudioHandler.chaos = currentValue; // References the Chaos amount for adaptive music purposes.

        if (slider.value == 1.0f)
        {
            //end.ShowMenu();
            //end.PauseGame();
        }
    }
    
    public void AddChaos(int modifier)
    {
        currentCombo += 1;
        if (currentCombo >= 2)
        {
            comboText.text = "x"+ currentCombo.ToString();
        }
        comboTimer += 20f;
        currentValue += 0.34f;
        if(currentValue >= 1.0f)
        {
            currentValue = 1.0f;
        }
        //AudioHandler.chaos = currentValue;
        Debug.Log("&*&" + currentValue);
        slider.value = currentValue;
    }
}
