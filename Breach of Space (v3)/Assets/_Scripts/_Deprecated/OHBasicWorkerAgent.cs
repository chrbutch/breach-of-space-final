using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OHBasicWorkerAgent : MonoBehaviour
{/*
    NavMeshAgent agent;
    public Interactable coffee;
    public Interactable sink;
    public Interactable myDesk;
    public float catchTimer = 0f;

    Queue<Interactable> LowPriorityTasks;
    Stack<Interactable> HighPriorityTasks;
    public PlayerMove player;

    //float amountWorked;
    bool isWorking;
    bool playeraware = false;
    /*public bool sliding;

    Interactable myCurrentTask;
    Vector3 lockDirect;
    bool canSee;

    public Material[] mat;
    Renderer rend;
    public bool playerAff = false;

    void Start() 
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = mat[0];

        agent = GetComponent<NavMeshAgent>();
        // initialize my tasks.
        LowPriorityTasks = new Queue<Interactable>();
        HighPriorityTasks = new Stack<Interactable>();
        // For the sake of testing, I'll make getting coffee be the new priority task.
        // Remember to PUSH and POP to/from a stack, ENQUEUE/DEQUEUE into a queue.
        HighPriorityTasks.Push(coffee);

        GetNextTask();
    }

    void GetNextTask() 
    {
        // if we don't have a task we're doing...
        // get one and unstop us.
        if (myCurrentTask is null)
        {
            if (HighPriorityTasks.Count > 0) myCurrentTask = HighPriorityTasks.Pop();
            else if (LowPriorityTasks.Count > 0) myCurrentTask = LowPriorityTasks.Dequeue();
            else 
            {
                float rFloat = Random.Range(0f, 1f);
                if (rFloat <= .6f) myCurrentTask = myDesk;
                else if (rFloat <= .9f) myCurrentTask = coffee;
                else myCurrentTask = sink;
            } 

            agent.destination = myCurrentTask.transform.position;
        }
        
    }
    private void OnTriggerEnter(Collider collider)
    {

        if(collider.CompareTag("Player"))
        {
            canSee = true;
        }
    }

    private bool LineofSight()
    {
        RaycastHit hit;
        var rayDirection = player.transform.position - transform.position;
        if (Physics.Raycast (transform.position, rayDirection, out hit, Mathf.Infinity)) 
        {
            if (hit.transform.tag == "Player") {
                Debug.Log("Can see player");
                return true;
            }
            else{
                Debug.Log("Can not see player");
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canSee = false;
            if (playeraware)
                {
                    playeraware = false;
                    player.awareDown();
                }
        }
    }
    public void AccomplishTask() 
    {
        // just wait two seconds and do the task.
        StartCoroutine(TakeTime(2));
        print("Accomplishing task.");
    }
 
    public void MessWithMe(int timeTaken) // Placeholder -- when we don't know what else to do.
    {
        print("I got messed with.");
        StartCoroutine(TakeTime(timeTaken));
    }
    public void MessWithMe(Interactable newTask, int timeTaken) 
    {
        // if we get messed with by something that requires a new task to be done...
        // make it a priority.
        // Also, give a delay.
        print("Messed with. Going to " + newTask.name);
        HighPriorityTasks.Push(newTask);
        StartCoroutine(TakeTime(timeTaken));
    }
    //TakeTime : called when performing any action.
    // Presumably timeTaken will be larger when sabotaged.
    // This is how we set the current task to null.
    IEnumerator TakeTime(int timeTaken) 
    {
        print("taking time.");
        yield return new WaitForSeconds(timeTaken);

        myCurrentTask = null;
        GetNextTask();

        agent.isStopped = false;
    }
    
    void Update() 
    {
        catchTimer -= Time.deltaTime;
        if (sliding)
        {
            transform.position += Vector3.forward * Time.deltaTime;
        }
        // if the agent is close enough and is walking, try to interact with the object.
        else if (agent.remainingDistance <= 1f && !agent.isStopped) 
        {
            print("We got close to " + myCurrentTask.name);
            print(agent.remainingDistance);
            // if we are successful, accomplish the task.
            if (myCurrentTask.AIInteract(this)) AccomplishTask();
            agent.isStopped = true;
        }
        else if (isWorking) 
        {
            // increment the amount worked.
        }
        if (canSee)
        {
            if (LineofSight())
            {
                if (!playeraware)
                {
                    playeraware = true;
                    player.awareUp();
                }
                if (player.sus && catchTimer <= 0)
                {
                    player.Caught();
                    catchTimer += 3.5f;
                }   
            }
            else
            {
                if (playeraware)
                {
                    playeraware = false;
                    player.awareDown();
                }
            }
        }
        if (playerAff == true)
        {
            rend.sharedMaterial = mat[1];
        }
        else
        {
            rend.sharedMaterial = mat[0];
        }
    }*/
}
