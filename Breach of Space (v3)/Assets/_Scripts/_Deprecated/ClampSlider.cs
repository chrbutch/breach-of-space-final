using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClampSlider : MonoBehaviour
{
    CanvasGroup canvas;
    Slider slider;
    //JobScript job;

    public GameObject placeholder;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<CanvasGroup>();
        slider = GetComponentInChildren<Slider>();
        //job = GetComponentInParent<JobScript>();
        canvas.alpha = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 slidePos = Camera.main.WorldToScreenPoint(placeholder.transform.position);
        slider.transform.position = slidePos;

        /*slider.value = job.workProgress;

        if(job.workProgress <= 0)
        {
            if(canvas.alpha > 0)
            {
                canvas.alpha -= Time.deltaTime;
            }
        }
        else
        {
            if(canvas.alpha < 1)
            {
                canvas.alpha += Time.deltaTime * 1.5f;
            } 
        }
        */
    }
}
