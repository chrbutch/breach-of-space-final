using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.AI;
using EPOOutline;

public class Viewfinder : MonoBehaviour
{
    public GameObject currentRoom;
    public GameObject currentCharacter;
    public GameObject currentObject;

    Camera mainCamera;
    Animator cameraAnimator;

    bool viewing_World;
    bool viewing_Room;
    bool viewing_Character;

    public CinemachineStateDrivenCamera stateCamera;
    public CinemachineVirtualCamera roomCAM;
    public CinemachineVirtualCamera charCAM;

    public GameObject roomCamAvatar;
    public GameObject charCamAvatar;

    public List<GameObject> allRoomsInLevel = new List<GameObject>();
    public List<GameObject> allCharactersInLevel = new List<GameObject>();

    Vector3 robotOffset = new Vector3(0, 2.2f, 0);

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GetComponent<Camera>();
        cameraAnimator = GetComponent<Animator>();
        foreach(RobotMovement robo in FindObjectsOfType<RobotMovement>())
        {
            allCharactersInLevel.Add(robo.gameObject);
            //Debug.Log(" **** " + robo.gameObject.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        cameraAnimator.SetBool("characterView", viewing_Character);
        cameraAnimator.SetBool("roomView"     , viewing_Room     );
        cameraAnimator.SetBool("worldView"    , viewing_World    );


        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Left mouse click.");

            Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] raycastHits = Physics.RaycastAll(cameraRay);

            //Debug.Log("Raycast hit: " + cameraHit.collider.gameObject);
            if (currentCharacter == null)
            {
                for (int b = 0; b < raycastHits.Length; b++)
                {
                    RaycastHit cameraHit = raycastHits[b];

                    //Debug.Log("Room = " + currentRoom.name + ": Searching for Robot");
                    //IF A ROOM IS SELECTED
                    if (cameraHit.collider.tag == "Robot")
                    {
                        currentCharacter = cameraHit.collider.gameObject;
                        viewing_Character = true;

                        charCamAvatar.transform.position =
                            currentCharacter.transform.position
                            + robotOffset;
                        // for CHAR CAM AVATAR: add + 2.2f to the Y to
                        //  make it follow character's head (technically,
                        //  it should just add height but this is a hack
                        //  until later...)
                        ScrollWheel.changingViews = true;
                        currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.green;
                        break;
                    }
                }
                /*
                //Debug.Log("Character == null");
                // IF NOT VIEWING A CHARACTER
                if (currentRoom == null)
                {

                    
                    for(int r = 0; r < raycastHits.Length; r++)
                    {
                        RaycastHit cameraHit = raycastHits[r];

                        if (cameraHit.collider.tag == "Room")
                        {
                            //Debug.Log("Hit Room: " + cameraHit.collider);
                            roomCamAvatar.transform.position = cameraHit.collider.transform.position + new Vector3(25, 25, 25); // <---- FIX ME.
                            currentRoom = cameraHit.collider.gameObject;
                            currentRoom.GetComponent<BoxCollider>().enabled = false;
                            viewing_Room = true;
                        }
                        else
                        {
                            // if not a room, then what????????????????
                            //Debug.Log("If not a room, then what?");
                        }
                    } 
                    
                }
                else
                {
                    
                    
                    /*
                    else if (cameraHit.collider.tag == "Room")
                    {
                        Debug.Log("ROOM: " + cameraHit.collider.gameObject);
                        currentRoom.GetComponent<BoxCollider>().enabled = true;
                        roomCamAvatar.transform.position = cameraHit.collider.transform.position;
                        currentRoom = cameraHit.collider.gameObject;
                        viewing_Room = true;
                        // CURRENTLY, DO WE NEED TO SELECT OTHER ROOMS?
                    }
                    
                }
                */
            }
            else
            {
                bool foundSomething = false;
                for (int j = 0; j < raycastHits.Length; j++)
                {
                    if (!foundSomething)
                    {
                        RaycastHit cameraHit = raycastHits[j];

                        if (cameraHit.collider.tag == "Job")
                        {
                            //Debug.Log("JOB FOUND: " + cameraHit.collider.name);

                            //currentCharacter.GetComponent<RobotMovement>().SetRobotDestination(
                              //  cameraHit.collider.GetComponentInParent<JobScript>().workingArea.transform.position);
                              //HEY, I DID THIS. -c.
                            foundSomething = true;
                        }
                        else if (cameraHit.collider.tag == "Robot")
                        {
                            currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.red;
                            currentCharacter = cameraHit.collider.gameObject;
                            viewing_Character = true;

                            charCamAvatar.transform.position =
                                currentCharacter.transform.position
                                + robotOffset;
                            currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.green;
                            foundSomething = true;
                            ScrollWheel.changingViews = true;
                            break;
                        }
                        else
                        {
                            //Nothing.
                        }
                    }
                }
                RaycastHit pointHit;
                NavMeshHit navMeshHit;/*
                if (Physics.Raycast(cameraRay, out pointHit) && !foundSomething)
                {
                    //Debug.Log("$$$ " + pointHit.collider.gameObject.name);
                    if (NavMesh.SamplePosition(pointHit.point, out navMeshHit, 5, NavMesh.AllAreas))
                    {
                        //Debug.Log(navMeshHit.position + " // " + pointHit.point);
                        currentCharacter.GetComponent<RobotMovement>().SetRobotDestination(navMeshHit.position);
                    }
                }*/

            }
        }

        /*
        if(currentCharacter != null)
        {
            charCamAvatar.transform.position = currentCharacter.transform.position;
            // change this when updating camera positions in-game?
        }
        */

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            ScrollWheel.changingViews = true;
            if(currentCharacter == null)
            {
                currentCharacter = allCharactersInLevel[0];
            }
            else
            {
                currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.red;
                int currentRobotIndex = allCharactersInLevel.IndexOf(currentCharacter);
                if(currentRobotIndex <= 0)
                {
                    currentCharacter = allCharactersInLevel[allCharactersInLevel.Count - 1];
                }
                else
                {
                    currentCharacter = allCharactersInLevel[currentRobotIndex - 1];
                }
            }
            /*
            if(currentCharacter == null)
            {
                if(currentRoom != null)
                {
                    currentRoom.GetComponent<BoxCollider>().enabled = true;
                    
                    int cur = allRoomsInLevel.IndexOf(currentRoom) - 1;
                    if(cur >= 0)
                    {
                        currentRoom = allRoomsInLevel[cur];
                    }
                    else
                    {
                        currentRoom = allRoomsInLevel[allRoomsInLevel.Count - 1];
                    }

                    currentRoom.GetComponent<BoxCollider>().enabled = false;
                }
            }
            else
            {
                int cur = currentRoom.GetComponent<RoomScript>().robotsInRoom.IndexOf(currentCharacter) - 1;
                int max = currentRoom.GetComponent<RoomScript>().robotsInRoom.Count;
                if(cur >= 0)
                {
                    currentCharacter = currentRoom.GetComponent<RoomScript>().robotsInRoom[cur];
                }
                else
                {
                    currentCharacter = currentRoom.GetComponent<RoomScript>().robotsInRoom[max - 1];
                }
            }
            */
            viewing_Character = true;
            charCamAvatar.transform.position =
                currentCharacter.transform.position
                + robotOffset;
            currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.green;
        }
        else if (Input.GetKeyDown(KeyCode.RightShift))
        {
            ScrollWheel.changingViews = true;
            if (currentCharacter == null)
            {
                currentCharacter = allCharactersInLevel[allCharactersInLevel.Count - 1];
            }
            else
            {
                currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.red;
                int currentRobotIndex = allCharactersInLevel.IndexOf(currentCharacter);
                if (currentRobotIndex >= allCharactersInLevel.Count - 1)
                {
                    currentCharacter = allCharactersInLevel[0];
                }
                else
                {
                    currentCharacter = allCharactersInLevel[currentRobotIndex + 1];
                }
            }
            viewing_Character = true;
            charCamAvatar.transform.position =
                currentCharacter.transform.position
                + robotOffset;
            currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.green;
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            ScrollWheel.changingViews = true;
            if (viewing_Character)
            {
                viewing_Character = false;
            }
            else
            {
                currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.red;
                currentCharacter = null;
            }

            /*
            if(currentCharacter != null)
            {
                currentCharacter.GetComponent<Outlinable>().FrontParameters.Color = Color.red;
                currentCharacter = null;
                viewing_Character = false;
            }
            else
            {
                if(currentRoom != null)
                {
                    //currentRoom.GetComponent<BoxCollider>().enabled = true;
                    currentRoom = null;
                }

                viewing_Room = false;
            }
            */
        }
    }
}
