using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SabotageMinigame : MonoBehaviour
{
    public GameObject targetTile;
    public GameObject highlightTargetTile;
    public GameObject baseTile;
    public GameObject highlightBaseTile;
    public GameObject backgroundPiece;
    public GameObject successPiece;
    public GameObject failurePiece;
    public List<GameObject> tiles = new List<GameObject>();
    public bool inTarget = false;
    public int curTarget = 1;
    public int targetStart =2;
    private bool goingUp = false;
    public bool runGame = false;
    private float timeLeft = 0.2f;
    public float timeLength = 0.07f;
    private GameObject curTile = null;
    private bool acceptable = false;
    private float curPosition = -70f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(runGame)
        {
            timeLeft -= Time.deltaTime;
            if(timeLeft <= 0)
            {
                timeLeft += timeLength;
                if(curTarget == 14 || curTarget == 1)
                {
                    goingUp = !goingUp;
                }
                if(goingUp)
                {
                    curPosition += 10.715f; 
                    curTarget += 1;
                    Destroy(curTile);
                    if(curTarget == targetStart)
                    {
                        inTarget = true;
                    }
                    else if (curTarget == (targetStart + 3))
                    {
                        inTarget = false;
                    }
                    if (inTarget)
                    {
                        GameObject tile = Instantiate(highlightTargetTile, new Vector3(0, 0, 0), Quaternion.identity);
                        curTile = tile;
                        tile.transform.SetParent(backgroundPiece.transform, false);
                        tile.transform.localPosition = new Vector3(curPosition, 10, -1f);
                        tile.transform.localScale = new Vector3(.15f, .75f, 1f);
                    }
                    else
                    {
                        GameObject tile = Instantiate(highlightBaseTile, new Vector3(0, 0, 0), Quaternion.identity);
                        curTile = tile;
                        tile.transform.parent = backgroundPiece.transform;
                        tile.transform.localPosition = new Vector3(curPosition, 10, -1f);
                        tile.transform.localScale = new Vector3(.15f, .75f, 1f);
                    }
                }
                else
                {
                    curPosition -= 10.715f; 
                    curTarget -= 1;
                    Destroy(curTile);
                    if(curTarget == (targetStart+2))
                    {
                        inTarget = true;
                    }
                    else if (curTarget == (targetStart - 1))
                    {
                        inTarget = false;
                    }
                    if (inTarget)
                    {
                        GameObject tile = Instantiate(highlightTargetTile, new Vector3(0, 0, 0), Quaternion.identity);
                        curTile = tile;
                        //tile.transform.parent = tiles[curTarget - 1].transform;
                        //tile.transform.localPosition = new Vector3(0, 0, -1);
                        //tile.transform.localScale = new Vector3(1f, 1f, 1f);
                        tile.transform.parent = backgroundPiece.transform;
                        tile.transform.localPosition = new Vector3(curPosition, 10, -1f);
                        tile.transform.localScale = new Vector3(.15f, .7f, 1f);
                    }
                    else
                    {
                        GameObject tile = Instantiate(highlightBaseTile, new Vector3(0, 0, 0), Quaternion.identity);
                        curTile = tile;
                        tile.transform.SetParent(backgroundPiece.transform);
                        tile.transform.localPosition = new Vector3(curPosition, 10, -1f);
                        tile.transform.localScale = new Vector3(.15f, .7f, 1f);
                    }
                }
            }
            bool triggered = false;
            if(acceptable)
            {
                if(Input.GetMouseButtonDown(0) && inTarget)
                {
                    Success();
                    Reset();
                    triggered = true;
                }
                else if(Input.GetMouseButtonDown(0) && !inTarget)
                {
                    Failure();
                    Reset();
                    triggered = true;
                }
            }
            if(!triggered)
            {
                acceptable = true;
            }
        }
    }
    void Reset()
    {
        backgroundPiece.SetActive(false);
        foreach(GameObject dtile in tiles)
        {
            Destroy(dtile);
        }
        tiles = new List<GameObject>();
        Destroy(curTile);
        goingUp = false;
        inTarget = false;
        curTarget = 1;
        runGame = false;
        acceptable = false;
        curPosition = -70f;
    }

    void Success()
    {
        successPiece.SetActive(true);
        GetComponent<Sabotage>().Break();
        
        AudioHandler.PlaySound("event:/s_Sabotage_Computer_01", new Vector3(11.5f, 1f, 30f));
        
        StartCoroutine(SuccessClose(1f));
        //get to at least here
        
    }

     IEnumerator SuccessClose(float time)
    {
        
        yield return new WaitForSeconds(time);
        print("IT HASN'T DIED YET");
        //don't get past here
        successPiece.SetActive(false);
    }

    public void Failure()
    {
        failurePiece.SetActive(true);
        AudioHandler.PlaySound("event:/s_Sabotage_Failure", new Vector3(11.5f, 1f, 30f));
        //failure case?????
        StartCoroutine(FailureClose(1f));
    }

    IEnumerator FailureClose(float time)
    {
        yield return new WaitForSeconds(time);
 
        failurePiece.SetActive(false);
    }

    public void SpawnGame()
    {
        if(!acceptable)
        {
            //print("runSpawn");
            backgroundPiece.SetActive(true);
            int tileCount = 0;
            int targets = 0;
            bool targeting = false;
            targetStart = Random.Range(0,12);
            float currentPosition = -70f;
            while(tileCount < 14)
            {
                if (tileCount == targetStart)
                {
                    targeting = true;
                } 
                if(targeting)
                {
                    GameObject btile = Instantiate(targetTile, new Vector3(0, 0, 0), Quaternion.identity);
                    btile.transform.SetParent(backgroundPiece.transform);
                    btile.transform.localPosition = new Vector3(currentPosition, 5, -6);
                    btile.transform.localScale = new Vector3(.15f, .75f, 1f);
                    tiles.Add(btile);
                    targets += 1;
                    if (targets == 3)
                    {
                        targeting = false;
                    }
                }
                else
                {
                    GameObject btile = Instantiate(baseTile, new Vector3(0, 0, 0), Quaternion.identity);
                    btile.transform.parent = backgroundPiece.transform;
                    btile.transform.localPosition = new Vector3(currentPosition, 0, -6);
                    btile.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);
                    tiles.Add(btile);
                }
                tileCount += 1;
                currentPosition += 10.715f;
            }
            GameObject ftile = Instantiate(highlightBaseTile, new Vector3(0, 0, 0), Quaternion.identity);
            curTile = ftile;
            ftile.transform.parent = tiles[0].transform;
            ftile.transform.localPosition = new Vector3(0, 5, -1);
            ftile.transform.localScale = new Vector3(.15f, .75f, 1f);
            runGame = true;
        }
    }
}
